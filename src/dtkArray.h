// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <type_traits>

#include "dtkArrayIterator.h"
#include "dtkBufferManager.h"
#include "dtkContainers.h"

namespace dtk {

// /////////////////////////////////////////////////////////////////
// Forward class declaration
// /////////////////////////////////////////////////////////////////

template < typename T > class ArrayMid;
template < typename T > class ArrayFinal;

// /////////////////////////////////////////////////////////////////
// dtk::ArrayBase class
// /////////////////////////////////////////////////////////////////

template < typename T > class ArrayBase
{
public:
    typedef T                  value_type;
    typedef value_type*        pointer;
    typedef const value_type*  const_pointer;
    typedef value_type&        reference;
    typedef const value_type&  const_reference;
    typedef std::ptrdiff_t     difference_type;
    typedef std::size_t        size_type;
    typedef BufferManager<T>   buffer_manager_type;

public:
    typedef  ArrayBase<T> ConstView;
    typedef   ArrayMid<T>      View;
    typedef ArrayFinal<T>    NoView;

protected:
    size_type m_capacity;
    size_type m_size;
    size_type m_stride;
    T* m_buffer;
    BufferManager<T> m_bm;

public:
    ArrayBase(void);
    ArrayBase(const T *buffer, size_type count, size_type stride = 1);
    ArrayBase(const ArrayBase& o);
    ArrayBase(ArrayBase&& o);

protected:
    ArrayBase(ArrayBase&& o, const BufferManager<T>& manager);

protected:
    ArrayBase(size_type capacity, size_type count, T *buffer, size_type stride, const BufferManager<T>& manager);

public:
    virtual ~ArrayBase(void);

public:
                              ArrayBase& operator = (const ArrayBase& o) = delete;
    template < typename RHS > ArrayBase& operator = (const       RHS& o) = delete;

public:
    bool operator == (const ArrayBase& o) const;
    bool operator != (const ArrayBase& o) const;

public:
    bool empty(void) const;

public:
    size_type     size(void) const;
    size_type capacity(void) const;
    size_type   stride(void) const;

public:
    size_type  count(void) const;
    size_type length(void) const;

public:
    const_reference at(size_type index) const;

public:
    const_reference operator[](size_type index) const;

public:
    const_pointer data(void) const;

public:
    buffer_manager_type bufferManager(void) const;

public:
    ConstView view(size_type from, size_type to, size_type stride = 1) const;

protected:
    void freeBuffer(T *& ptr, size_t size);
};

// /////////////////////////////////////////////////////////////////

template < typename T > using ConstArrayView = const ArrayBase<T>;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template < typename T > class ArrayMid : public ArrayBase<T>
{
public:
    typedef T                  value_type;
    typedef value_type*        pointer;
    typedef const value_type*  const_pointer;
    typedef value_type&        reference;
    typedef const value_type&  const_reference;
    typedef std::ptrdiff_t     difference_type;
    typedef std::size_t        size_type;
    typedef BufferManager<T>   buffer_manager_type;

public:
    typedef ConstArrayView<T> ConstView;
    typedef       ArrayMid<T>      View;
    typedef     ArrayFinal<T>    NoView;

protected:
    typedef ArrayBase<T> Base;

    using Base::m_capacity;
    using Base::m_size;
    using Base::m_stride;
    using Base::m_buffer;
    using Base::m_bm;

public:
    ArrayMid(void);
    ArrayMid(T *buffer, size_type count, size_type stride = 1);
    ArrayMid(const ArrayMid& o);
    ArrayMid(ArrayMid&& o);

protected:
    ArrayMid(ArrayMid&& o, const BufferManager<T>& manager);

protected:
    ArrayMid(const BufferManager<T>& manager, size_type capacity, size_type count, T *buffer, size_type stride = 1);

public:
    ~ArrayMid(void);

public:
    ArrayMid& operator = (const ArrayBase<T>& o) = delete;
    ArrayMid& operator = (const     ArrayMid& o);

public:
    reference operator[](size_type index);

public:
    void fill(const T& value);

public:
    const_pointer data(void) const;
          pointer data(void);

public:
    ConstView view(size_type from, size_type to, size_type stride = 1) const;
         View view(size_type from, size_type to, size_type stride = 1);
};

// /////////////////////////////////////////////////////////////////

template < typename T > using ArrayView = ArrayMid<T>;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template < typename T > class ArrayFinal : public ArrayMid<T>
{
public:
    typedef ArrayIterator<T>            iterator;
    typedef ArrayConstIterator<T> const_iterator;

public:
    typedef std::reverse_iterator<iterator>		        reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

public:
    typedef T                  value_type;
    typedef value_type*        pointer;
    typedef const value_type*  const_pointer;
    typedef value_type&        reference;
    typedef const value_type&  const_reference;
    typedef std::ptrdiff_t     difference_type;
    typedef std::size_t        size_type;
    typedef BufferManager<T>   buffer_manager_type;

public:
    typedef ConstArrayView<T> ConstView;
    typedef      ArrayView<T>      View;
    typedef     ArrayFinal<T>    NoView;

protected:
    typedef typename ArrayMid<T>::Base Base;

    using Base::m_capacity;
    using Base::m_size;
    using Base::m_stride;
    using Base::m_buffer;
    using Base::m_bm;

public:
             ArrayFinal(void);
    explicit ArrayFinal(const BufferManager<T>& manager);
    explicit ArrayFinal(size_type count, const BufferManager<T>& manager = BufferManager<T>());
    explicit ArrayFinal(size_type count, const T& value, const BufferManager<T>& manager = BufferManager<T>());

public:
    ArrayFinal(const ArrayFinal& o);
    ArrayFinal(const ArrayFinal& o, const BufferManager<T>& manager);

public:
    ArrayFinal(ArrayFinal&& o);
    ArrayFinal(ArrayFinal&& o, const BufferManager<T>& manager);

public:
    ArrayFinal(std::initializer_list<T> list, const BufferManager<T>& manager = BufferManager<T>());

public:
    template <typename Iterator, typename Enable = typename containers::is_input_iterator<Iterator> > ArrayFinal(Iterator first, Iterator last, const BufferManager<T>& manager = BufferManager<T>());

public:
    ArrayFinal(const T *buffer, size_type count, const BufferManager<T>& manager = BufferManager<T>());

public:
    ~ArrayFinal(void);

public:
    ArrayFinal& operator = (const   ArrayFinal& o);
    ArrayFinal& operator = (const ArrayBase<T>& o);

public:
    ArrayFinal& operator = (ArrayFinal&& o);

public:
    ArrayFinal& operator = (std::initializer_list<T> list);

public:
    using Base::empty;
    using Base::size;
    using Base::capacity;
    using Base::stride;
    using Base::count;
    using Base::length;
    using Base::operator==;
    using Base::operator!=;

public:
    void swap(ArrayFinal& o);

public:
    void   clear(void);

    void  resize(size_type size);
    void  resize(size_type size, const T& value);

    void reserve(size_type size);

    void shrink_to_fit(void);
    void       squeeze(void);

public:
    void fill(const T& value);

public:
    void insert(size_type index, const T& value);
    void insert(size_type index, size_type count, const T& value);

    iterator insert(const_iterator pos, const T& value);
    iterator insert(const_iterator pos, T&& value);
    iterator insert(const_iterator pos, size_type count, const T& value);

    template < typename Iterator, typename Enable = typename containers::is_input_iterator<Iterator> >
    iterator insert(const_iterator pos, Iterator first, Iterator last);

    iterator insert(const_iterator pos, std::initializer_list<T> list);

public:
    void append(const T& value);
    void append(T&& value);
    void append(const ArrayBase<T>& o);
    void append(std::initializer_list<T> list);
    void append(const T *values, size_type length);

    void prepend(const T& value);

public:
    void push_back(const T&  value);
    void push_back(      T&& value);

public:
    ArrayFinal& operator += (const T& value);
    ArrayFinal& operator += (const ArrayBase<T>& o);
    ArrayFinal& operator += (std::initializer_list<T> list);

    ArrayFinal& operator << (const T& value);
    ArrayFinal& operator << (const ArrayBase<T>& o);
    ArrayFinal& operator << (std::initializer_list<T> list);

public:
    template < typename... Args > iterator emplace(const_iterator pos, Args&&... args);

    template < typename... Args > void emplace_back(Args&&... args);

public:
    iterator erase(const_iterator pos);
    iterator erase(const_iterator first, const_iterator last);

    void pop_back(void);

    void remove(size_type index);
    void remove(size_type index, size_type count);

    void removeFirst(void);
    void  removeLast(void);

    value_type takeFirst(void);
    value_type  takeLast(void);
    value_type    takeAt(size_type index);

public:
    const_reference at(size_type index) const;

    const_reference front(void) const;
          reference front(void)      ;

    const_reference back(void) const;
          reference back(void)      ;

    const_reference first(void) const;
          reference first(void)      ;

    const_reference last(void) const;
          reference last(void)      ;

public:
    const_reference operator[](size_type index) const;
          reference operator[](size_type index)      ;

public:
    void setAt(size_type index, const T& value);
    void setAt(size_type index, const T *values, size_type length);

public:
          iterator  begin(void)      ;
    const_iterator  begin(void) const;
    const_iterator cbegin(void) const;

          iterator    end(void)      ;
    const_iterator    end(void) const;
    const_iterator   cend(void) const;

public:
          reverse_iterator  rbegin(void)      ;
    const_reverse_iterator  rbegin(void) const;
    const_reverse_iterator crbegin(void) const;

          reverse_iterator    rend(void)      ;
    const_reverse_iterator    rend(void) const;
    const_reverse_iterator   crend(void) const;

public:
    const_pointer data(void) const;
          pointer data(void);

    const_pointer constData(void) const;

public:
    template < typename U > typename std::enable_if<std::is_scalar<U>::value && std::is_convertible<U, T>::value>::type setRawData(U *buffer, size_type count, const BufferManager<U>& manager = BufferManager<U>());

public:
    void extractRawData(T *& buffer, size_type& count, BufferManager<T>& manager);

public:
    void setStaticRawData(T *buffer, size_type count);

public:
    ConstView constView(size_type from, size_type to, size_type stride = 1) const;

    ConstView view(size_type from, size_type to, size_type stride = 1) const;
         View view(size_type from, size_type to, size_type stride = 1);

public:
    bool startsWith(const T& value) const;
    bool   endsWith(const T& value) const;
    bool   contains(const T& value) const;

    size_type indexOf(const T& value) const;
    size_type indexOf(const T& value, size_type from) const;

    size_type lastIndexOf(const T& value) const;
    size_type lastIndexOf(const T& value, size_type from) const;

    size_type count(const T& value) const;
};

// /////////////////////////////////////////////////////////////////

template < typename T > using Array = ArrayFinal<T>;

// /////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////

template < typename T > typename std::enable_if< std::is_arithmetic<T>::value, std::ostream>::type& operator << (std::ostream& out, const ArrayBase<T>& x);
template < typename T > typename std::enable_if<!std::is_arithmetic<T>::value, std::ostream>::type& operator << (std::ostream& out, const ArrayBase<T>& x);

// /////////////////////////////////////////////////////////////////
// Implementations
// /////////////////////////////////////////////////////////////////

#include "dtkArray.tpp"

}; // dtk namespace ends here

//
// dtkArray.h ends here
