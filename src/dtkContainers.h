// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <iostream>
#include <iterator>
#include <type_traits>
#include <cstdint>

#include "dtkContainersExport.h"

namespace dtk
{
    class MemoryResourceFactory;

    namespace containers {

        namespace memory_resource {
            DTKCONTAINERS_EXPORT MemoryResourceFactory& pluginFactory(void);
            DTKCONTAINERS_EXPORT void setPolicy(const std::string& policy);
            DTKCONTAINERS_EXPORT const std::string& policy(void);
        }

        DTKCONTAINERS_EXPORT std::uint64_t roundUpNextPowerOfTwo(std::uint64_t nalloc);

    }
}

// /////////////////////////////////////////////////////////////////
// Typetraits
// /////////////////////////////////////////////////////////////////

namespace dtk {

    namespace containers {

        template < typename Iterator > using is_input_iterator = typename std::enable_if<std::is_convertible<typename std::iterator_traits<Iterator>::iterator_category, std::input_iterator_tag>::value>::type;

    }
}

//
// dtkContainers.h ends here
