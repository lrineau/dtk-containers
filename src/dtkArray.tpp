// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkContainersConfig.h"
#include "dtkMemoryResource.h"

// /////////////////////////////////////////////////////////////////
// dtk::ArrayBase implementation
// /////////////////////////////////////////////////////////////////

template < typename T > inline ArrayBase<T>::ArrayBase(void) : m_capacity(0), m_size(0), m_stride(1), m_buffer(nullptr), m_bm(BufferManager<T>("dtkMemoryResourceNull"))
{
}

template < typename T > inline ArrayBase<T>::ArrayBase(const T *buffer, size_type count, size_type stride) : m_capacity(count), m_size(count), m_stride(stride), m_buffer(const_cast<T *>(buffer)), m_bm(BufferManager<T>("dtkMemoryResourceNull"))
{
}

template < typename T > inline ArrayBase<T>::ArrayBase(const ArrayBase& o) : m_capacity(o.capacity()), m_size(o.size()), m_stride(o.stride()), m_buffer(const_cast<T *>(o.data())), m_bm(BufferManager<T>("dtkMemoryResourceNull"))
{
}

template < typename T > inline ArrayBase<T>::ArrayBase(ArrayBase&& o) : m_capacity(std::move(o.m_capacity)), m_size(std::move(o.m_size)), m_stride(std::move(o.m_stride)), m_buffer(std::move(o.m_buffer)), m_bm(std::move(o.m_bm))
{
}

template < typename T > inline ArrayBase<T>::ArrayBase(ArrayBase&& o, const BufferManager<T>& manager) : m_capacity(std::move(o.m_capacity)), m_size(std::move(o.m_size)), m_stride(std::move(o.m_stride)), m_buffer(std::move(o.m_buffer)), m_bm(manager)
{
}

template < typename T > inline ArrayBase<T>::ArrayBase(size_type capacity, size_type count, T *buffer, size_type stride, const BufferManager<T>& manager) : m_capacity(capacity), m_size(count), m_stride(stride), m_buffer(buffer), m_bm(manager)
{
}

template < typename T > inline ArrayBase<T>::~ArrayBase(void)
{
    this->freeBuffer(m_buffer, m_size);

    m_capacity = m_size = m_stride = 0;
}

template < typename T > inline bool ArrayBase<T>::operator == (const ArrayBase& o) const
{
    if (this == &o)
        return true;

    return (m_size == o.m_size && std::equal(m_buffer, m_buffer + m_size, o.m_buffer));
}

template < typename T > inline bool ArrayBase<T>::operator != (const ArrayBase& o) const
{
    return !(this->operator==(o));
}

template < typename T > inline bool ArrayBase<T>::empty(void) const
{
    return !m_size;
}

template < typename T > inline typename ArrayBase<T>::size_type ArrayBase<T>::size(void) const
{
    return m_size;
}

template < typename T > inline typename ArrayBase<T>::size_type ArrayBase<T>::capacity(void) const
{
    return m_capacity;
}

template < typename T > inline typename ArrayBase<T>::size_type ArrayBase<T>::stride(void) const
{
    return m_stride;
}

template < typename T > inline typename ArrayBase<T>::size_type ArrayBase<T>::count(void) const
{
    return this->size();
}

template < typename T > inline typename ArrayBase<T>::size_type ArrayBase<T>::length(void) const
{
    return this->size();
}

template < typename T > inline typename ArrayBase<T>::const_reference ArrayBase<T>::at(size_type index) const
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayBase<T>::at(index): index out of range.");

    return (*this)[index];
}

template < typename T > inline typename ArrayBase<T>::const_reference ArrayBase<T>::operator[](size_type index) const
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayBase<T>::operator[](index): index out of range.");

    return m_buffer[m_stride * index];
}

template < typename T > inline typename ArrayBase<T>::const_pointer ArrayBase<T>::data(void) const
{
    return m_buffer;
}

template < typename T > inline typename ArrayBase<T>::buffer_manager_type ArrayBase<T>::bufferManager(void) const
{
    return m_bm;
}

template < typename T > inline void ArrayBase<T>::freeBuffer(T *& ptr, size_t size)
{
    if (ptr) {
        if (m_bm.resource()->identifier() != "MemoryResourceNull") {
            m_bm.clearBuffer(ptr, ptr + size);
            m_bm.deallocate(ptr);
        }
        ptr = nullptr;
    }
}

// /////////////////////////////////////////////////////////////////
// ArrayBase view
// /////////////////////////////////////////////////////////////////

template < typename T > inline typename ArrayBase<T>::ConstView ArrayBase<T>::view(size_type from, size_type to, size_type stride) const
{
    DTKCONTAINERS_ASSERT(((from < to) && (from < m_size) && (to <= m_size) && (stride > 0)), "ArrayBase<T>::view() const: indices from and to are not valid.");

    const size_type length = ((to - from) / stride) + 1;

    pointer ptr = &(this->operator[](from));

    return ConstView(ptr, length, m_stride * stride);
}

// /////////////////////////////////////////////////////////////////
// dtk::ArrayMid implementation
// /////////////////////////////////////////////////////////////////

template < typename T > inline ArrayMid<T>::ArrayMid(void) : ArrayBase<T>()
{
}

template < typename T > inline ArrayMid<T>::ArrayMid(T *buffer, size_type count, size_type stride) : ArrayBase<T>(count, count, buffer, stride, BufferManager<T>("dtkMemoryResourceNull"))
{
}

template < typename T > inline ArrayMid<T>::ArrayMid(const ArrayMid& o) : ArrayBase<T>(o)
{
}

template < typename T > inline ArrayMid<T>::ArrayMid(ArrayMid&& o) : ArrayBase<T>(std::move(o))
{
}

template < typename T > inline ArrayMid<T>::ArrayMid(ArrayMid&& o, const BufferManager<T>& manager) : ArrayBase<T>(std::move(o), manager)
{
}

template < typename T > inline ArrayMid<T>::ArrayMid(const BufferManager<T>& manager, size_type capacity, size_type count, T *buffer, size_type stride) : ArrayBase<T>(capacity, count, buffer, stride, manager)
{
}

template < typename T > inline ArrayMid<T>::~ArrayMid(void)
{
}

template < typename T > inline ArrayMid<T>& ArrayMid<T>::operator = (const ArrayMid& o)
{
    if (this != &o) {
        m_buffer   = o.m_buffer;
        m_size     = o.m_size;
        m_capacity = o.m_size;
        m_stride   = o.m_stride;
        m_bm       = BufferManager<T>("dtkMemoryResourceNull");
    }
    return *this;
}

template < typename T > inline typename ArrayMid<T>::reference ArrayMid<T>::operator[](size_type index)
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayMid<T>::operator[](index): index out of range.");

    return m_buffer[m_stride * index];
}

template < typename T > inline void ArrayMid<T>::fill(const T& value)
{
    for(size_type i = 0; i < m_size; ++i) {
        this->operator[](i) = value;
    }
}

template < typename T > inline typename ArrayMid<T>::const_pointer ArrayMid<T>::data(void) const
{
    return m_buffer;
}

template < typename T > inline typename ArrayMid<T>::pointer ArrayMid<T>::data(void)
{
    return m_buffer;
}

// /////////////////////////////////////////////////////////////////
// ArrayMid views
// /////////////////////////////////////////////////////////////////

template < typename T > inline typename ArrayMid<T>::ConstView ArrayMid<T>::view(size_type from, size_type to, size_type stride) const
{
    DTKCONTAINERS_ASSERT(((from < to) && (from < m_size) && (to <= m_size) && (stride > 0)), "ArrayMid<T>::view() const: indices from and to are not valid.");

    const size_type length = ((to - from) / stride) + 1;

    const_pointer ptr = &(this->operator[](from));

    return ConstView(ptr, length, m_stride * stride);
}

template < typename T > inline typename ArrayMid<T>::View ArrayMid<T>::view(size_type from, size_type to, size_type stride)
{
    DTKCONTAINERS_ASSERT(((from < to) && (from < m_size) && (to <= m_size) && (stride > 0)), "ArrayMid<T>::view(): indices from and to are not valid.");

    const size_type length = ((to - from) / stride) + 1;

    pointer ptr = &(this->operator[](from));

    return View(ptr, length, m_stride * stride);
}

// /////////////////////////////////////////////////////////////////
// dtk::ArrayFinal implementation
// /////////////////////////////////////////////////////////////////

template < typename T > inline ArrayFinal<T>::ArrayFinal(void) : ArrayFinal(BufferManager<T>())
{
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(const BufferManager<T>& manager) : ArrayMid<T>(manager, 0UL, 0UL, nullptr)
{
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(size_type count, const BufferManager<T>& manager) : ArrayMid<T>(manager, count, count, const_cast<BufferManager<T>&>(manager).allocate(count))
{
    std::uninitialized_fill(m_buffer, m_buffer + m_size, T());
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(size_type count, const T& value, const BufferManager<T>& manager) : ArrayMid<T>(manager, count, count, const_cast<BufferManager<T>&>(manager).allocate(count))
{
    std::uninitialized_fill(m_buffer, m_buffer + m_size, value);
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(const ArrayFinal& o) : ArrayMid<T>(o.m_bm, o.m_size, o.m_size, const_cast<BufferManager<T>&>(o.m_bm).allocate(o.m_size))
{
    std::uninitialized_copy(o.m_buffer, o.m_buffer + o.m_size, m_buffer);
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(const ArrayFinal& o, const BufferManager<T>& manager) : ArrayMid<T>(manager, o.m_size, o.m_size, manager.allocate(o.m_size))
{
    std::uninitialized_copy(o.m_buffer, o.m_buffer + o.m_size, m_buffer);
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(ArrayFinal&& o) : ArrayMid<T>(std::move(o))
{
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(ArrayFinal&& o, const BufferManager<T>& manager) : ArrayMid<T>(std::move(o), manager)
{
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(std::initializer_list<T> list, const BufferManager<T>& manager) : ArrayFinal(list.begin(), list.end(), manager)
{
}

template < typename T > template < typename Iterator, typename Enable > inline ArrayFinal<T>::ArrayFinal(Iterator first, Iterator last, const BufferManager<T>& manager) : ArrayMid<T>(manager, std::distance(first, last), std::distance(first, last), const_cast<BufferManager<T>&>(manager).allocate(std::distance(first, last)))
{
    static_assert(std::is_convertible<typename std::iterator_traits<Iterator>::value_type, value_type>::value, "Input iterator's value_type is not convertible into container's value_type");

    std::uninitialized_copy(first, last, m_buffer);
}

template < typename T > inline ArrayFinal<T>::ArrayFinal(const T *buffer, size_type count, const BufferManager<T>& manager) : ArrayFinal(buffer, buffer + count, manager)
{
}

template < typename T > inline ArrayFinal<T>::~ArrayFinal(void)
{
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator = (const ArrayFinal& o)
{
    if (this != &o) {

        if (o.m_size > m_capacity) {

            T *ptr = m_bm.allocate(o.m_size);

            this->freeBuffer(m_buffer, m_size);

            m_buffer = ptr;
            m_capacity = o.m_size;

        } else if (o.m_size < m_size) {
            m_bm.clearBuffer(m_buffer + o.m_size, m_buffer + m_size);
        }

        m_size = o.m_size;

        m_bm.copyBuffer(o.m_buffer, o.m_buffer + m_size, m_buffer);
    }

    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator = (const ArrayBase<T>& o)
{
    if (this != &o) {

        if (o.size() > m_capacity) {

            T *ptr = m_bm.allocate(o.size());

            this->freeBuffer(m_buffer, m_size);

            m_buffer = ptr;
            m_capacity = o.size();

        } else if (o.size() < m_size) {
            m_bm.clearBuffer(m_buffer + o.size(), m_buffer + m_size);
        }

        m_size = o.size();

        for(size_type i = 0; i < m_size; ++i) {
            m_buffer[i] = o[i];
        }
    }

    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator = (ArrayFinal&& o)
{
    this->swap(o);

    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator = (std::initializer_list<T> list)
{

}

template < typename T > inline void ArrayFinal<T>::swap(ArrayFinal& o)
{
    if (this != &o) {
        std::swap(m_capacity, o.m_capacity);
        std::swap(m_size, o.m_size);
        std::swap(m_stride, o.m_stride);
        std::swap(m_buffer, o.m_buffer);
        std::swap(m_bm, o.m_bm);
    }
}

template < typename T > inline void ArrayFinal<T>::clear(void)
{
    m_bm.clearBuffer(m_buffer, m_buffer + m_size);
    m_size = 0;
}

template < typename T > inline void ArrayFinal<T>::resize(size_type size)
{
    if (size > m_capacity) {
        // May adjust new size to fit with packed requirements
        T *ptr = m_bm.allocate(size);

        if (m_buffer) {
            m_bm.copyBuffer(m_buffer, m_buffer + m_size, ptr);
        }

        if (std::is_fundamental<T>::value) {
            m_bm.initBuffer(ptr + m_size, ptr + size);
        }

        std::swap(m_buffer, ptr);

        this->freeBuffer(ptr, m_size);

        m_capacity = size;

    } else if (size < m_size) {
        m_bm.clearBuffer(m_buffer + size, m_buffer + m_size);
    }

    m_size = size;
}

template < typename T > inline void ArrayFinal<T>::resize(size_type size, const T& value)
{
    if (size > m_capacity) {
        // May adjust new size to fit with packed requirements
        T *ptr = m_bm.allocate(size);

        if (m_buffer) {
            m_bm.copyBuffer(m_buffer, m_buffer + m_size, ptr);
        }

        m_bm.initBuffer(ptr + m_size, ptr + size, value);

        this->freeBuffer(m_buffer, m_size);

        m_buffer = ptr;
        m_capacity = size;

    } else if (size < m_size) {
        m_bm.clearBuffer(m_buffer + size, m_buffer + m_size);

    } else { // m_size < size < m_capacity
         m_bm.initBuffer(m_buffer + m_size, m_buffer + size, value);
    }

    m_size = size;
}

template < typename T > inline void ArrayFinal<T>::reserve(size_type size)
{
    if (size > this->capacity()) {
        pointer ptr = m_bm.allocate(size);
        m_bm.copyBuffer(m_buffer, m_buffer + m_size, ptr);
        this->freeBuffer(m_buffer, m_size);
        m_buffer = ptr;
        m_capacity = size;
    }
}

template < typename T > inline void ArrayFinal<T>::shrink_to_fit(void)
{
    if( this->capacity() == this->size() )
        return;

    ArrayFinal<T> tmp(m_buffer, m_buffer + m_size, m_bm);
    tmp.swap(*this);
}

template < typename T > inline void ArrayFinal<T>::squeeze(void)
{
    this->shrink_to_fit();
}

template < typename T > inline void ArrayFinal<T>::fill(const T& value)
{
    std::fill(this->begin(), this->end(), value);
}

template < typename T > inline void ArrayFinal<T>::insert(size_type index, const T& value)
{
    DTKCONTAINERS_ASSERT((index <= this->size()), "ArrayFinal<T>::insert(): index out of range");

    this->insert(this->cbegin() + index, 1, value);
}

template < typename T > inline void ArrayFinal<T>::insert(size_type index, size_type count, const T& value)
{
    DTKCONTAINERS_ASSERT((index <= this->size()), "ArrayFinal<T>::insert(): index out of range");

    this->insert(this->cbegin() + index, count, value);
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::insert(const_iterator pos, const T& value)
{
    return this->emplace(pos, value);
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::insert(const_iterator pos, T&& value)
{
    return this->emplace(pos, std::move(value));
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::insert(const_iterator pos, size_type count, const T& value)
{
    DTKCONTAINERS_ASSERT((this->cbegin() <= pos) && (pos <= this->cend()), "ArrayFinal<T>::insert(): Iterator pos is invalid");

	const size_type offset = std::distance(this->cbegin(), pos);

    if (count != 0) {

        const value_type copy = value;

        pointer     before = m_buffer + offset;
        pointer old_finish = m_buffer + m_size;

        if ((m_capacity - m_size) >= count) {

            const size_type elements_after = std::distance(pos, this->cend());

            if (elements_after > count) {
                std::uninitialized_copy(old_finish - count, old_finish, old_finish);
                std::move_backward(before, old_finish - count, old_finish);
                std::fill(before, before + count, copy);

            } else {
                auto it = std::uninitialized_fill_n(old_finish, count - elements_after, copy);
                std::uninitialized_copy(before, old_finish, it);
                std::fill(before, old_finish, copy);
            }
            m_size += count;

        } else {

            const size_type new_capacity = containers::roundUpNextPowerOfTwo(m_size + count);
            pointer start = m_bm.allocate(new_capacity);
            pointer finish(start);

            try {
                std::uninitialized_fill_n(start + offset, count, copy);
                finish = pointer();

                finish = std::uninitialized_copy(m_buffer, before, start);
                finish += count;

                finish = std::uninitialized_copy(before, old_finish, finish);

            } catch(...) {
                if (!finish) {
                    m_bm.clearBuffer(start + offset, start + offset + count);
                } else {
                    m_bm.clearBuffer(start, finish);
                }
                m_bm.deallocate(start);

                std::rethrow_exception(std::current_exception());
            }

            std::swap(m_buffer, start);

            this->freeBuffer(start, m_size);
            m_capacity = new_capacity;
            m_size += count;
        }
    }

    return (this->begin() + offset);
}

template < typename T > template < typename Iterator, typename Enable > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::insert(const_iterator pos, Iterator first, Iterator last)
{
    DTKCONTAINERS_ASSERT((this->cbegin() <= pos) && (pos <= this->cend()), "ArrayFinal<T>::insert(): Iterator pos is invalid");

	const size_type offset = std::distance(this->cbegin(), pos);

    if (first != last) {

        const size_type count = std::distance(first, last);

        pointer before     = m_buffer + offset;
        pointer old_finish = m_buffer + m_size;

        if ((m_capacity - m_size) >= count) {

            const size_type elements_after = std::distance(pos, this->cend());

            if (elements_after > count) {
                std::uninitialized_copy(old_finish - count, old_finish, old_finish);
                std::move_backward(before, old_finish - count, old_finish);
                std::copy(first, last, before);

            } else {
                Iterator mid = first;
                std::advance(mid, elements_after);
                std::uninitialized_copy(mid, last, old_finish);
                auto it = old_finish + count - elements_after;
                std::uninitialized_copy(before, old_finish, it);
                std::copy(first, mid, before);
            }
            m_size += count;

        } else {

            const size_type new_capacity = containers::roundUpNextPowerOfTwo(m_size + count);
            pointer start = m_bm.allocate(new_capacity);
            pointer finish(start);

            try {
                finish = std::uninitialized_copy(m_buffer, before, start);
                finish = std::uninitialized_copy(first, last, finish);
                finish = std::uninitialized_copy(before, old_finish, finish);

            } catch(...) {
                m_bm.clearBuffer(start, finish);
                m_bm.deallocate(start);
                std::rethrow_exception(std::current_exception());
            }

            std::swap(m_buffer, start);
            this->freeBuffer(start, m_size);
            m_capacity = new_capacity;
            m_size += count;
        }
    }

    return this->begin() + offset;
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::insert(const_iterator pos, std::initializer_list<T> list)
{
    return this->insert(pos, list.begin(), list.end());
}

template < typename T > inline void ArrayFinal<T>::append(const T& value)
{
    this->emplace_back(value);
}

template < typename T > inline void ArrayFinal<T>::append(T&& value)
{
    this->emplace_back(std::move(value));
}

template < typename T > inline void ArrayFinal<T>::append(std::initializer_list<T> list)
{
    this->insert(this->cend(), list.begin(), list.end());
}

template < typename T > inline void ArrayFinal<T>::append(const ArrayBase<T>& o)
{
    this->insert(this->cend(), o.data(), o.data() + o.size());
}

template < typename T > inline void ArrayFinal<T>::append(const T *values, size_type length)
{
    this->insert(this->cend(), values, values + length);
}

template < typename T > inline void ArrayFinal<T>::prepend(const T& value)
{
    this->insert(this->cbegin(), value);
}

template < typename T > inline void ArrayFinal<T>::push_back(const T& value)
{
    this->emplace_back(value);
}

template < typename T > inline void ArrayFinal<T>::push_back(T&& value)
{
    this->emplace_back(std::move(value));
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator += (const T& value)
{
    this->append(value);
    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator += (const ArrayBase<T>& o)
{
    this->append(o);
    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator += (std::initializer_list<T> list)
{
    this->append(list);
    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator << (const T& value)
{
    this->append(value);
    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator << (const ArrayBase<T>& o)
{
    this->append(o);
    return *this;
}

template < typename T > inline ArrayFinal<T>& ArrayFinal<T>::operator << (std::initializer_list<T> list)
{
    this->append(list);
    return *this;
}

template < typename T > template < typename... Args > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::emplace(const_iterator pos, Args&&... args)
{
    DTKCONTAINERS_ASSERT((this->cbegin() <= pos) && (pos <= this->cend()), "ArrayFinal<T>::emplace(): Iterator pos is invalid");

    const size_type offset = std::distance(this->cbegin(), pos);

    if (m_size != m_capacity && offset == m_size) {
        m_bm.construct(&(*this->end()), std::forward<Args>(args)...);
        ++m_size;

    } else {

        pointer     before = m_buffer + offset;
        pointer old_finish = m_buffer + m_size;

        if (m_size != m_capacity) {
            m_bm.construct(old_finish, *(old_finish - 1));
            std::move_backward(before, old_finish - 1, old_finish);
            *before = T(std::forward<Args>(args)...);
            ++m_size;

        } else {

            const size_type new_capacity = containers::roundUpNextPowerOfTwo(m_size + 1);
            pointer start = m_bm.allocate(new_capacity);
            pointer finish(start);

            try {
                m_bm.construct(start + offset, std::forward<Args>(args)...);
                finish = pointer();

                finish = std::uninitialized_copy(m_buffer, before, start);
                ++finish;

                finish = std::uninitialized_copy(before, old_finish, finish);

            } catch(...) {
                if (!finish) {
                    m_bm.clearBuffer(start + offset, start + offset + 1);
                } else {
                    m_bm.clearBuffer(start, finish);
                }
                m_bm.deallocate(start);

                std::rethrow_exception(std::current_exception());
            }

            std::swap(m_buffer, start);

            this->freeBuffer(start, m_size);
            m_capacity = new_capacity;
            ++m_size;
        }
    }

    return this->begin() + offset;
}

template < typename T > template < typename... Args > inline void ArrayFinal<T>::emplace_back(Args&&... args)
{
    if (m_size == m_capacity) {

        const size_type new_capacity = containers::roundUpNextPowerOfTwo(m_size + 1);
        pointer start = m_bm.allocate(new_capacity);
        pointer finish(start);

        try {
            m_bm.construct(start + m_size, std::forward<Args>(args)...);

            finish = pointer();
            finish = std::uninitialized_copy(m_buffer, m_buffer + m_size, start);
            ++finish;

        } catch(...) {
            if (!finish) {
                m_bm.clearBuffer(start + new_capacity, start + new_capacity + 1);
            } else {
                m_bm.clearBuffer(start, finish);
            }
            m_bm.deallocate(start);

            std::rethrow_exception(std::current_exception());
        }

        std::swap(m_buffer, start);

        this->freeBuffer(start, m_size);
        m_capacity = new_capacity;
        ++m_size;

    } else {
        m_bm.construct(m_buffer + m_size, std::forward<Args>(args)...);
        ++m_size;
    }
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::erase(const_iterator pos)
{
    DTKCONTAINERS_ASSERT((this->cbegin() <= pos) && (pos < this->cend()), "ArrayFinal<T>::erase(): Iterator pos is invalid");

    size_type offset = std::distance(this->cbegin(), pos);

    pointer     before = m_buffer + offset;
    pointer old_finish = m_buffer + m_size;

    if (offset + 1 != m_size) {
        std::move(before + 1, old_finish, before);
    }

    m_bm.clearBuffer(old_finish - 1, old_finish);
    --m_size;

    return (this->begin() + offset);
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::erase(const_iterator first, const_iterator last)
{
    DTKCONTAINERS_ASSERT((this->cbegin() <= first) && (first <  this->cend()), "ArrayFinal<T>::erase(): Iterator first is invalid");
    DTKCONTAINERS_ASSERT((this->cbegin() <=  last) && ( last <= this->cend()), "ArrayFinal<T>::erase(): Iterator  last is invalid");

    size_type preset = std::distance(this->cbegin(), first);
    size_type offset = std::distance(this->cbegin(),  last);

    pointer     before = m_buffer + preset;
    pointer      after = m_buffer + offset;
    pointer old_finish = m_buffer + m_size;

    if (first != last) {

        if (offset != m_size) {
            std::move(after, old_finish, before);
        }

        m_bm.clearBuffer(before + (m_size - offset), old_finish);
        m_size -= offset - preset;
    }

    return (this->begin() + preset);
}

template < typename T > inline void ArrayFinal<T>::pop_back(void)
{
    DTKCONTAINERS_ASSERT((!this->empty()), "ArrayFinal<T>::pop_back(): array is empty.");

    pointer ptr = m_buffer + m_size;
    m_bm.clearBuffer(ptr - 1, ptr);
    --m_size;
}

template < typename T > inline void ArrayFinal<T>::remove(size_type index)
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::remove(index): index out of range.");

    this->erase(this->cbegin() + index);
}

template < typename T > inline void ArrayFinal<T>::remove(size_type index, size_type count)
{
    DTKCONTAINERS_ASSERT((index + count <= this->size()), "ArrayFinal<T>::remove(): index + count out of range");

    const_iterator pos = this->cbegin() + index;
    this->erase(pos, pos + count);
}

template < typename T > inline void ArrayFinal<T>::removeFirst(void)
{
    DTKCONTAINERS_ASSERT((!this->empty()), "ArrayFinal<T>::removeFirst(): array is empty.");

    this->erase(this->cbegin());
}

template < typename T > inline void ArrayFinal<T>::removeLast(void)
{
    DTKCONTAINERS_ASSERT((!this->empty()), "ArrayFinal<T>::removeLast(): array is empty.");

    this->erase(this->cend() - 1);
}

template < typename T > inline typename ArrayFinal<T>::value_type ArrayFinal<T>::takeFirst(void)
{
    DTKCONTAINERS_ASSERT((!this->empty()), "ArrayFinal<T>::takeFirst(): array is empty.");

    auto cit = this->cbegin();

    value_type res = *cit; this->erase(cit);

    return res;
}

template < typename T > inline typename ArrayFinal<T>::value_type ArrayFinal<T>::takeLast(void)
{
    DTKCONTAINERS_ASSERT((!this->empty()), "ArrayFinal<T>::takeLast(): array is empty.");

    auto cit = this->cend() - 1;

    value_type res = *cit; this->erase(cit);

    return res;
}

template < typename T > inline typename ArrayFinal<T>::value_type ArrayFinal<T>::takeAt(size_type index)
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::takeAt(index): index out of range.");

    auto cit = this->cbegin() + index;

    value_type res = *cit; this->erase(cit);

    return res;
}

template < typename T > inline typename ArrayFinal<T>::const_reference ArrayFinal<T>::at(size_type index) const
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::at(index): index out of range.");

    return (*this)[index];
}

template < typename T > inline typename ArrayFinal<T>::const_reference ArrayFinal<T>::front(void) const
{
    return *(this->cbegin());
}

template < typename T > inline typename ArrayFinal<T>::reference ArrayFinal<T>::front(void)
{
    return *(this->begin());
}

template < typename T > inline typename ArrayFinal<T>::const_reference ArrayFinal<T>::back(void) const
{
    return *(this->cend() - 1);
}

template < typename T > inline typename ArrayFinal<T>::reference ArrayFinal<T>::back(void)
{
    return *(this->end() - 1);
}

template < typename T > inline typename ArrayFinal<T>::const_reference ArrayFinal<T>::first(void) const
{
    return *(this->cbegin());
}

template < typename T > inline typename ArrayFinal<T>::reference ArrayFinal<T>::first(void)
{
    return *(this->begin());
}

template < typename T > inline typename ArrayFinal<T>::const_reference ArrayFinal<T>::last(void) const
{
    return *(this->cend() - 1);
}

template < typename T > inline typename ArrayFinal<T>::reference ArrayFinal<T>::last(void)
{
    return *(this->end() - 1);
}

template < typename T > inline typename ArrayFinal<T>::const_reference ArrayFinal<T>::operator[](size_type index) const
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::operator[](index): index out of range.");

    return m_buffer[index];
}

template < typename T > inline typename ArrayFinal<T>::reference ArrayFinal<T>::operator[](size_type index)
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::operator[](index): index out of range.");

    return m_buffer[index];
}

template < typename T > inline void ArrayFinal<T>::setAt(size_type index, const T& value)
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::setAt(): index out of range.");

    (*this)[index] = value;
}

template < typename T > inline void ArrayFinal<T>::setAt(size_type index, const T *values, size_type length)
{
    DTKCONTAINERS_ASSERT((index < this->size()), "ArrayFinal<T>::setAt(): index out of range.");
    DTKCONTAINERS_ASSERT((index + length <= this->size()), "ArrayFinal<T>::setAt(): index + length out of range.");

    std::copy(values, values + length, m_buffer + index);
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::begin(void)
{
    return iterator(m_buffer);
}

template < typename T > inline typename ArrayFinal<T>::const_iterator ArrayFinal<T>::begin(void) const
{
    return const_iterator(m_buffer);
}

template < typename T > inline typename ArrayFinal<T>::const_iterator ArrayFinal<T>::cbegin(void) const
{
    return const_iterator(m_buffer);
}

template < typename T > inline typename ArrayFinal<T>::iterator ArrayFinal<T>::end(void)
{
    return iterator(m_buffer + m_size);
}

template < typename T > inline typename ArrayFinal<T>::const_iterator ArrayFinal<T>::end(void) const
{
    return const_iterator(m_buffer + m_size);
}

template < typename T > inline typename ArrayFinal<T>::const_iterator ArrayFinal<T>::cend(void) const
{
    return const_iterator(m_buffer + m_size);
}

template < typename T > inline typename ArrayFinal<T>::reverse_iterator ArrayFinal<T>::rbegin(void)
{
    return reverse_iterator(this->end());
}

template < typename T > inline typename ArrayFinal<T>::const_reverse_iterator ArrayFinal<T>::rbegin(void) const
{
    return const_reverse_iterator(this->end());
}

template < typename T > inline typename ArrayFinal<T>::const_reverse_iterator ArrayFinal<T>::crbegin(void) const
{
    return const_reverse_iterator(this->end());
}

template < typename T > inline typename ArrayFinal<T>::reverse_iterator ArrayFinal<T>::rend(void)
{
    return reverse_iterator(this->begin());
}

template < typename T > inline typename ArrayFinal<T>::const_reverse_iterator ArrayFinal<T>::rend(void) const
{
    return const_reverse_iterator(this->begin());
}

template < typename T > inline typename ArrayFinal<T>::const_reverse_iterator ArrayFinal<T>::crend(void) const
{
    return const_reverse_iterator(this->begin());
}

template < typename T > inline typename ArrayFinal<T>::const_pointer ArrayFinal<T>::data(void) const
{
    return m_buffer;
}

template < typename T > inline typename ArrayFinal<T>::pointer ArrayFinal<T>::data(void)
{
    return m_buffer;
}

template < typename T > inline typename ArrayFinal<T>::const_pointer ArrayFinal<T>::constData(void) const
{
    return m_buffer;
}

template < typename T > template < typename U > inline typename std::enable_if<std::is_scalar<U>::value && std::is_convertible<U, T>::value>::type ArrayFinal<T>::setRawData(U *buffer, size_type count, const BufferManager<U>& manager)
{
    if(buffer != m_buffer) {
        this->freeBuffer(m_buffer, m_size);
        m_buffer = buffer;
        m_capacity = count;
        m_size = count;
        m_bm = manager;

    } else {
        m_size = count;
    }
}

template < typename T > inline void ArrayFinal<T>::extractRawData(T *& buffer, size_type& count, BufferManager<T>& manager)
{
    buffer  = m_buffer;
    count   = m_size;
    manager = m_bm;

    m_buffer = nullptr;
    m_size   = 0;
    m_capacity = 0;

    m_bm = BufferManager<T>("dtkMemoryResourceNull");
}

template < typename T > inline void ArrayFinal<T>::setStaticRawData(T *buffer, size_type count)
{
    if(buffer != m_buffer) {
        this->freeBuffer(m_buffer, m_size);
        m_buffer = buffer;
        m_capacity = count;
        m_size = count;
        m_bm = BufferManager<T>("dtkMemoryResourceNull");

    } else {
        m_size = count;
    }
}

// /////////////////////////////////////////////////////////////////
// Array views
// /////////////////////////////////////////////////////////////////

template < typename T > inline typename ArrayFinal<T>::ConstView ArrayFinal<T>::constView(size_type from, size_type to, size_type stride) const
{
    DTKCONTAINERS_ASSERT(((from < to) && (from < m_size) && (to <= m_size) && (stride > 0)), "ArrayFinal<T>::constView(): indices from and to are not valid.");

    const size_type length = (to - from) / stride + 1;

    const_pointer ptr = &(this->operator[](from));

    return ConstView(ptr, length, stride);
}

template < typename T > inline typename ArrayFinal<T>::ConstView ArrayFinal<T>::view(size_type from, size_type to, size_type stride) const
{
    DTKCONTAINERS_ASSERT(((from < to) && (from < m_size) && (to <= m_size) && (stride > 0)), "ArrayFinal<T>::view() const: indices from and to are not valid.");

    const size_type length = (to - from) / stride + 1;

    const_pointer ptr = &(this->operator[](from));

    return ConstView(ptr, length, stride);
}

template < typename T > inline typename ArrayFinal<T>::View ArrayFinal<T>::view(size_type from, size_type to, size_type stride)
{
    DTKCONTAINERS_ASSERT(((from < to) && (from < m_size) && (to <= m_size) && (stride > 0)), "ArrayFinal<T>::view(): indices from and to are not valid.");

    const size_type length = (to - from) / stride + 1;

    pointer ptr = &(this->operator[](from));

    return View(ptr, length, stride);
}

// /////////////////////////////////////////////////////////////////
// Convenient methods
// /////////////////////////////////////////////////////////////////

template < typename T > inline bool ArrayFinal<T>::startsWith(const T& value) const
{
    return (!this->empty() && this->front() == value);
}

template < typename T > inline bool ArrayFinal<T>::endsWith(const T& value) const
{
    return (!this->empty() && this->back() == value);
}

template < typename T > inline bool ArrayFinal<T>::contains(const T& value) const
{
    auto res = std::find(this->begin(), this->end(), value);
    return (res != this->end());
}

template < typename T > inline typename ArrayFinal<T>::size_type ArrayFinal<T>::indexOf(const T& value) const
{
    return this->indexOf(value, size_type(0));
}

template < typename T > inline typename ArrayFinal<T>::size_type ArrayFinal<T>::indexOf(const T& value, size_type from) const
{
    DTKCONTAINERS_ASSERT((from >= 0) && (from < m_size), "ArrayFinal<T>::indexOf(): index from must be greater than 0 and lower than size.");

    auto start = this->cbegin() + from;

    auto res = std::find(start, this->cend(), value);

    size_type index = -1;
    if (res != this->end())
        index = std::distance(this->begin(), res);

    return index;
}

template < typename T > inline typename ArrayFinal<T>::size_type ArrayFinal<T>::lastIndexOf(const T& value) const
{
    return this->lastIndexOf(value, this->size() - 1);
}

template < typename T > inline typename ArrayFinal<T>::size_type ArrayFinal<T>::lastIndexOf(const T& value, size_type from) const
{
    DTKCONTAINERS_ASSERT((from >= 0) && (from < m_size), "ArrayFinal<T>::lastIndexOf(): index from must be greater than 0 and lower than size.");

    auto start = this->begin() + from + 1;
    auto  last = this->begin();

    while(start != last) {
        if (*(--start) == value)
            return std::distance(last, start);
    }
    return -1;
}

template < typename T > inline typename ArrayFinal<T>::size_type ArrayFinal<T>::count(const T& value) const
{
    return std::count(this->begin(), this->end(), value);
}

// /////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////

template < typename T > inline typename std::enable_if<std::is_arithmetic<T>::value, std::ostream>::type& operator << (std::ostream& out, const ArrayBase<T>& x)
{
    typedef typename ArrayBase<T>::size_type size_type;

    out << "Array of size " << std::to_string(x.size()) << " :" << std::endl;

    out << "[ ";
    for (size_type i = 0; i != x.size(); ++i) {
        if (i)
            out << ", ";

        out << std::to_string(x[i]);
    }
    out << " ]";
    return out;
}

template < typename T > inline typename std::enable_if<!std::is_arithmetic<T>::value, std::ostream>::type& operator << (std::ostream& out, const ArrayBase<T>& x)
{
    typedef typename ArrayBase<T>::size_type size_type;

    out << "Array of size " << std::to_string(x.size()) << " :" << std::endl;

    out << "[ ";
    for (size_type i = 0; i != x.size(); ++i) {
        if (i)
            out << ", ";

        out << x[i];
    }
    out << " ]";

    return out;
}

//
// dtkArray.tpp ends here
