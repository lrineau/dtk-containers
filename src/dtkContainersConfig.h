// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <cstdlib>
#include <cassert>

namespace dtk {

    namespace containers {

        inline void assert_yes(bool cond, const char *msg) {
            if(!cond) std::cerr << msg << std::endl;
        }

        inline void assert_no(bool, const char *) {}
    }
}

#ifndef NDEBUG
#define DTKCONTAINERS_ASSERT(cond, msg) dtk::containers::assert_yes(cond, msg); assert(cond);
#else
#define DTKCONTAINERS_ASSERT(cond, msg) dtk::containers::assert_no(cond, msg);
#endif

//
// dtkContainersConfig.h ends here
