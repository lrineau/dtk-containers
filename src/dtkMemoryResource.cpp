// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMemoryResource.h"
#include "dtkContainersPluginFactory.h"

#include <memory>
#include <stdlib.h>
#include <iostream>

namespace dtk {

// /////////////////////////////////////////////////////////////////
// MemoryResourceFactory implementation
// /////////////////////////////////////////////////////////////////

    MemoryResourceFactory::MemoryResourceFactory(void) : ContainersPluginFactory<MemoryResource>(), m_policy("dtkMemoryResourceAlign")
{
    this->record("dtkMemoryResourceAlign",         &MemoryResourceAlignCreator);
    this->record("dtkMemoryResourceNull",          &MemoryResourceNullCreator);
    this->record("dtkMemoryResourceMallocAndFree", &MemoryResourceMallocAndFreeCreator);
    this->record("dtkMemoryResourceNewAndDelete",  &MemoryResourceNewAndDeleteCreator);
}

MemoryResourceFactory::~MemoryResourceFactory(void)
{
}

void MemoryResourceFactory::setPolicy(const std::string& policy)
{
    m_policy = policy;
}

const std::string& MemoryResourceFactory::policy(void) const
{
    return m_policy;
}

// /////////////////////////////////////////////////////////////////
// MemoryResourceAlignHandler implementation
// /////////////////////////////////////////////////////////////////

void *MemoryResourceAlignHandler::allocate(std::size_t bytes, std::size_t alignment)
{
    void *raw(nullptr);

#if defined(_MSC_VER)
    raw =_aligned_malloc(bytes, alignment);
    if(raw == nullptr)
#else
    if(posix_memalign(&raw, alignment, bytes))
#endif
        throw std::bad_alloc();

    return raw;
}

void *MemoryResourceAlignHandler::deallocate(void *ptr, std::size_t, std::size_t)
{
#if defined(_MSC_VER)
    _aligned_free(ptr);
#else
    free(ptr);
#endif
    return nullptr;
}

// /////////////////////////////////////////////////////////////////
// MemoryResourceAlign implementation
// /////////////////////////////////////////////////////////////////

void *MemoryResourceAlign::allocate(std::size_t bytes, std::size_t alignment)
{
    if (alignment >= 8UL) {

        return MemoryResourceAlignHandler::allocate(bytes, alignment);

    } else {

        return (new unsigned char[bytes]);
    }
}

void *MemoryResourceAlign::deallocate(void *ptr, std::size_t bytes, std::size_t alignment)
{
    if(ptr == nullptr)
        return ptr;

    if (alignment >= 8UL) {

        return MemoryResourceAlignHandler::deallocate(ptr, bytes, alignment);

    } else {

        unsigned char *d = static_cast<unsigned char *>(ptr);
        delete [] d;
        return nullptr;

    }
}

bool MemoryResourceAlign::equal(const MemoryResource& other) const
{
    return (this == &other);
}

std::string MemoryResourceAlign::identifier(void) const
{
    return "MemoryResourceAlign";
}

// /////////////////////////////////////////////////////////////////
// MemoryResourceMallocAndFree implementation
// /////////////////////////////////////////////////////////////////

void *MemoryResourceMallocAndFree::allocate(std::size_t bytes, std::size_t)
{
    void *raw(nullptr);
    raw = std::malloc(bytes);
    if(raw == nullptr)
        throw std::bad_alloc();

    return raw;
}

void *MemoryResourceMallocAndFree::deallocate(void *ptr, std::size_t, std::size_t)
{
    std::free(ptr);
    return nullptr;
}

bool MemoryResourceMallocAndFree::equal(const MemoryResource& other) const
{
    return this == &other;
}

std::string MemoryResourceMallocAndFree::identifier(void) const
{
    return "MemoryResourceMallocAndFree";
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

void *MemoryResourceNewAndDelete::allocate(std::size_t bytes, std::size_t)
{
    return (new unsigned char[bytes]);
}

void *MemoryResourceNewAndDelete::deallocate(void *ptr, std::size_t, std::size_t)
{
    unsigned char *d = static_cast<unsigned char *>(ptr);
    delete [] d;
    return nullptr;
}

bool MemoryResourceNewAndDelete::equal(const MemoryResource& other) const
{
    return this == &other;
}

std::string MemoryResourceNewAndDelete::identifier(void) const
{
    return "MemoryResourceNewAndDelete";
}

} // dtk namespace ends here

//
// dtkMemoryResource.cpp ends here
