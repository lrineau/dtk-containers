// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <cstddef>
#include <memory>
#include <utility>

#include "dtkMemoryResource.h"

namespace dtk {

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template < typename T > class BufferManager
{
public:
    BufferManager(void);
    BufferManager(const std::string& memory_resource_policy);
    BufferManager(MemoryResourcePointer mem_r);
    BufferManager(const BufferManager& o) = default;
    BufferManager(BufferManager&& o);

public:
    template < typename U > BufferManager(const BufferManager<U>& other);

public:
    ~BufferManager(void);

public:
    BufferManager& operator = (const BufferManager& o);

public:
    template < typename U > BufferManager& operator = (const BufferManager<U>& other);

public:
    T *allocate(std::size_t n);

    void deallocate(T *ptr, std::size_t n = 0);

public:
    MemoryResourcePointer resource(void) const;

public:
    template < typename U, typename... Args > void construct(U *ptr, Args&&... args);
    template < typename U >                   void   destroy(U *ptr);

public:
    template <typename U, typename... Args > typename std::enable_if< std::is_fundamental<U>::value>::type initBuffer(U *from, U *to, Args&&... args);
    template <typename U, typename... Args > typename std::enable_if<!std::is_fundamental<U>::value>::type initBuffer(U *from, U *to, Args&&... args);

public:
    template <typename U > typename std::enable_if< std::is_fundamental<U>::value>::type copyBuffer(const U *s_from, const U *s_to, U *t_from);
    template <typename U > typename std::enable_if<!std::is_fundamental<U>::value>::type copyBuffer(const U *s_from, const U *s_to, U *t_from);

public:
    template <typename U > typename std::enable_if< std::is_fundamental<U>::value>::type clearBuffer(U *from, U *to);
    template <typename U > typename std::enable_if<!std::is_fundamental<U>::value>::type clearBuffer(U *from, U *to);

protected:
    MemoryResourcePointer mem_resource;
};


// /////////////////////////////////////////////////////////////////
// Helper Functions
// /////////////////////////////////////////////////////////////////

template < typename T1, typename T2 > bool operator == (const BufferManager<T1>& lhs, const BufferManager<T2>& rhs)
{
    return lhs.resource()->equal( *(rhs.resource()) );
}

template < typename T1, typename T2 > bool operator != (const BufferManager<T1>& lhs, const BufferManager<T2>& rhs)
{
    return !(lhs == rhs);
}

}; // dtk namespace ends here

// /////////////////////////////////////////////////////////////////

#include "dtkBufferManager.tpp"

//
// dtkBufferManager.h ends here
