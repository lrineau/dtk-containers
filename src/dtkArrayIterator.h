// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

namespace dtk {

// /////////////////////////////////////////////////////////////////
// ArrayIterator
// /////////////////////////////////////////////////////////////////

template <typename T> struct ArrayIterator {
public:
    T *i;
    typedef std::random_access_iterator_tag iterator_category;
    typedef std::intptr_t difference_type;
    typedef T value_type;
    typedef T* pointer;
    typedef T& reference;

public:
    ArrayIterator(void) : i(0) {}
    ArrayIterator(T *n) : i(n) {}

public:
    T& operator *  (void) const { return *i; }
    T *operator -> (void) const { return  i; }
    T& operator [] (difference_type j) const { return *(i + j); }
    bool operator == (const ArrayIterator& o) const { return i == o.i; }
    bool operator != (const ArrayIterator& o) const { return i != o.i; }
    bool operator <  (const ArrayIterator& o) const { return i <  o.i; }
    bool operator <= (const ArrayIterator& o) const { return i <= o.i; }
    bool operator >  (const ArrayIterator& o) const { return i >  o.i; }
    bool operator >= (const ArrayIterator& o) const { return i >= o.i; }
    ArrayIterator&  operator ++ (void) { ++i; return *this; }
    ArrayIterator   operator ++ (int) { T *n = i; ++i; return n; }
    ArrayIterator&  operator -- (void) { i--; return *this; }
    ArrayIterator   operator -- (int) { T *n = i; i--; return n; }
    ArrayIterator&  operator += (difference_type j) { i+=j; return *this; }
    ArrayIterator&  operator -= (difference_type j) { i-=j; return *this; }
    ArrayIterator   operator +  (difference_type j) const { return ArrayIterator(i+j); }
    ArrayIterator   operator -  (difference_type j) const { return ArrayIterator(i-j); }
    difference_type operator -  (const ArrayIterator& j) const { return i - j.i; }
    operator T* (void) const { return i; }
};

// /////////////////////////////////////////////////////////////////
// ArrayConstIterator
// /////////////////////////////////////////////////////////////////

template <typename T> struct ArrayConstIterator {
public:
    const T *i;
    typedef std::random_access_iterator_tag  iterator_category;
    typedef std::intptr_t difference_type;
    typedef T value_type;
    typedef const T* pointer;
    typedef const T& reference;

public:
    ArrayConstIterator(void) : i(0) {}
    ArrayConstIterator(const T *n) : i(n) {}
    ArrayConstIterator(const ArrayIterator<T>& o): i(o.i) {}

public:
    const T& operator *  (void) const { return *i; }
    const T *operator -> (void) const { return  i; }
    const T& operator [] (difference_type j) const { return *(i + j); }
    bool operator == (const ArrayConstIterator& o) const { return i == o.i; }
    bool operator != (const ArrayConstIterator& o) const { return i != o.i; }
    bool operator <  (const ArrayConstIterator& o) const { return i <  o.i; }
    bool operator <= (const ArrayConstIterator& o) const { return i <= o.i; }
    bool operator >  (const ArrayConstIterator& o) const { return i >  o.i; }
    bool operator >= (const ArrayConstIterator& o) const { return i >= o.i; }
    ArrayConstIterator &operator ++ (void) { ++i; return *this; }
    ArrayConstIterator operator  ++ (int) { const T *n = i; ++i; return n; }
    ArrayConstIterator &operator -- (void) { i--; return *this; }
    ArrayConstIterator operator  -- (int) { const T *n = i; i--; return n; }
    ArrayConstIterator &operator += (difference_type j) { i+=j; return *this; }
    ArrayConstIterator &operator -= (difference_type j) { i-=j; return *this; }
    ArrayConstIterator operator  +  (difference_type j) const { return ArrayConstIterator(i+j); }
    ArrayConstIterator operator  -  (difference_type j) const { return ArrayConstIterator(i-j); }
    difference_type    operator  -  (const ArrayConstIterator& j) const { return i - j.i; }
    operator const T* (void) const { return i; }
};

} // end of the dtk namespace

//
// dtkArrayArrayIterator.h ends here
