// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkContainers.h"
#include "dtkContainersConfig.h"
#include "dtkMemoryResource.h"

namespace dtk {

    namespace containers {

        namespace memory_resource {
            namespace _private {
                MemoryResourceFactory factory;
            }

            MemoryResourceFactory& pluginFactory(void) {
                return _private::factory;
            }

            void setPolicy(const std::string& policy) {
                _private::factory.setPolicy(policy);
            }

            const std::string& policy(void) {
                return _private::factory.policy();
            }
        }

        std::uint64_t roundUpNextPowerOfTwo(std::uint64_t nalloc)
        {
            DTKCONTAINERS_ASSERT(nalloc < (std::uint64_t(1) << 63) + 1, "Requested size is too large!");

            nalloc |= nalloc >> 1;
            nalloc |= nalloc >> 2;
            nalloc |= nalloc >> 4;
            nalloc |= nalloc >> 8;
            nalloc |= nalloc >> 16;
            nalloc |= nalloc >> 32;
            ++nalloc;

            return nalloc;
        }
    }
}

//
// dtkContainers.cpp ends here
