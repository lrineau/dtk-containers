// Version: $Id$
//
//

// Commentary: FAKE CLASS FOR DOCUMENTATION ONLY
//
//

// Change Log:
//
//

// Code:


template < typename T > class dtkArray
{
public:
    typedef dtkArrayIterator<T>            iterator;
    typedef dtkArrayConstIterator<T> const_iterator;

public:
    typedef std::reverse_iterator<iterator>		        reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

public:
    typedef T                  value_type;
    typedef value_type*        pointer;
    typedef const value_type*  const_pointer;
    typedef value_type&        reference;
    typedef const value_type&  const_reference;
    typedef std::ptrdiff_t     difference_type;
    typedef std::size_t        size_type;

public:
    typedef dtkBufferManager<T>   buffer_manager_type;

public:
    typedef dtkConstArrayView<T> ConstView;
    typedef      dtkArrayView<T>      View;
    typedef          dtkArray<T>    NoView;

public:
             dtkArray(void);
    explicit dtkArray(const dtkBufferManager<T>& manager);
    explicit dtkArray(size_type count, const dtkBufferManager<T>& manager = dtkBufferManager<T>());
    explicit dtkArray(size_type count, const T& value, const dtkBufferManager<T>& manager = dtkBufferManager<T>());

public:
    dtkArray(const dtkArray& o);
    dtkArray(const dtkArray& o, const dtkBufferManager<T>& manager);

public:
    dtkArray(dtkArray&& o);
    dtkArray(dtkArray&& o, const dtkBufferManager<T>& manager);

public:
    dtkArray(std::initializer_list<T> list, const dtkBufferManager<T>& manager = dtkBufferManager<T>());

public:
    template <typename Iterator, typename Enable = typename containers::is_input_iterator<Iterator> > dtkArray(Iterator first, Iterator last, const dtkBufferManager<T>& manager = dtkBufferManager<T>());

public:
    dtkArray(const T *buffer, size_type count, const dtkBufferManager<T>& manager = dtkBufferManager<T>());

public:
    ~dtkArray(void);

public:
    dtkArray& operator = (const dtkArray& o);
    dtkArray& operator = (const dtkArrayView& o);
    dtkArray& operator = (const dtkConstArrayView& o);

public:
    dtkArray& operator = (dtkArray&& o);

public:
    dtkArray& operator = (std::initializer_list<T> list);

public:
    bool operator == (const dtkArray& o) const;
    bool operator != (const dtkArray& o) const;

public:
    bool empty(void) const;

public:
    size_type     size(void) const;
    size_type capacity(void) const;
    size_type   stride(void) const;

public:
    size_type  count(void) const;
    size_type length(void) const;

public:
    void swap(dtkArray& o);

public:
    void   clear(void);

    void  resize(size_type count);
    void  resize(size_type count, const T& value);

    void reserve(size_type new_cap);

    void shrink_to_fit(void);
    void       squeeze(void);

public:
    void fill(const T& value);

public:
    void insert(size_type index, const T& value);
    void insert(size_type index, size_type count, const T& value);

    iterator insert(const_iterator pos, const T& value);
    iterator insert(const_iterator pos, T&& value);
    iterator insert(const_iterator pos, size_type count, const T& value);

    template < typename Iterator, typename Enable = typename containers::is_input_iterator<Iterator> >
    iterator insert(const_iterator pos, Iterator first, Iterator last);

    iterator insert(const_iterator pos, std::initializer_list<T> list);

public:
    void append(const T& value);
    void append(T&& value);
    void append(const dtkArray<T>& o);
    void append(std::initializer_list<T> list);
    void append(const T *values, size_type length);

    void prepend(const T& value);

public:
    void push_back(const T&  value);
    void push_back(      T&& value);

public:
    dtkArray& operator += (const T& value);
    dtkArray& operator += (const dtkArray<T>& o);
    dtkArray& operator += (std::initializer_list<T> list);

    dtkArray& operator << (const T& value);
    dtkArray& operator << (const dtkArray<T>& o);
    dtkArray& operator << (std::initializer_list<T> list);

public:
    template < typename... Args > iterator emplace(const_iterator pos, Args&&... args);

    template < typename... Args > void emplace_back(Args&&... args);

public:
    iterator erase(const_iterator pos);
    iterator erase(const_iterator first, const_iterator last);

    void pop_back(void);

    void remove(size_type index);
    void remove(size_type index, size_type count);

    void removeFirst(void);
    void  removeLast(void);

    value_type takeFirst(void);
    value_type  takeLast(void);
    value_type    takeAt(size_type index);

public:
    const_reference at(size_type index) const;

    const_reference front(void) const;
          reference front(void)      ;

    const_reference back(void) const;
          reference back(void)      ;

    const_reference first(void) const;
          reference first(void)      ;

    const_reference last(void) const;
          reference last(void)      ;

public:
    const_reference operator[](size_type index) const;
          reference operator[](size_type index)      ;

public:
    void setAt(size_type index, const T& value);
    void setAt(size_type index, const T *values, size_type length);

public:
          iterator  begin(void)      ;
    const_iterator  begin(void) const;
    const_iterator cbegin(void) const;

          iterator    end(void)      ;
    const_iterator    end(void) const;
    const_iterator   cend(void) const;

public:
          reverse_iterator  rbegin(void)      ;
    const_reverse_iterator  rbegin(void) const;
    const_reverse_iterator crbegin(void) const;

          reverse_iterator    rend(void)      ;
    const_reverse_iterator    rend(void) const;
    const_reverse_iterator   crend(void) const;

public:
    const_pointer data(void) const;
          pointer data(void);

    const_pointer constData(void) const;

public:
    template < typename U > typename std::enable_if<std::is_scalar<U>::value && std::is_convertible<U, T>::value>::type setRawData(U *buffer, size_type count, const dtkBufferManager<U>& manager = dtkBufferManager<U>());

public:
    void extractRawData(T *& buffer, size_type& count, dtkBufferManager<T>& manager);

public:
    void setStaticRawData(T *buffer, size_type count);

public:
    ConstView constView(size_type from, size_type to, size_type stride = 1) const;

    ConstView view(size_type from, size_type to, size_type stride = 1) const;
         View view(size_type from, size_type to, size_type stride = 1);

public:
    buffer_manager_type bufferManager(void) const;

public:
    bool startsWith(const T& value) const;
    bool   endsWith(const T& value) const;
    bool   contains(const T& value) const;

    size_type indexOf(const T& value) const;
    size_type indexOf(const T& value, size_type from) const;

    size_type lastIndexOf(const T& value) const;
    size_type lastIndexOf(const T& value, size_type from) const;

    size_type count(const T& value) const;
};

// /////////////////////////////////////////////////////////////////

template < typename T > typename std::enable_if< std::is_arithmetic<T>::value, std::ostream>::type& operator << (std::ostream& out, const dtkArray<T>& x);
template < typename T > typename std::enable_if<!std::is_arithmetic<T>::value, std::ostream>::type& operator << (std::ostream& out, const dtkArray<T>& x);

//
// dtkArray.h ends here
