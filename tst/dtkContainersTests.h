// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtTest>

#define DTKCONTAINERSTEST_MAIN(TestMain, TestObject)	        \
    int TestMain(int argc, char *argv[])			\
    {                                               \
        QApplication app(argc, argv);               \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);		\
    }

#define DTKCONTAINERSTEST_MAIN_NOGUI(TestMain, TestObject)	\
    int TestMain(int argc, char *argv[])			\
    {                                               \
        QCoreApplication app(argc, argv);           \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);       \
    }

//
// dtkContainersTests.h ends here
