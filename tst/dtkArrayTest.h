// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include "dtkContainersTests.h"

class dtkArrayTestCase : public QObject
{
    Q_OBJECT

public:
             dtkArrayTestCase(void);
    virtual ~dtkArrayTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreateBasics(void);
    void testCreateListAndBufferAndRange(void);
    void testCopyAndAssignement(void);
    void testSwap(void);
    void testClear(void);
    void testResize(void);
    void testReserve(void);
    void testSqueezeAndShrink(void);
    void testFill(void);
    void testInsertValue(void);
    void testInsertRange(void);
    void testInsertComplexType(void);
    void testAppend(void);
    void testAppendArray(void);
    void testAppendList(void);
    void testPushBack(void);
    void testEmplaceAndEmplaceBack(void);
    void testErase(void);
    void testPopBack(void);
    void testRemove(void);
    void testTake(void);
    void testAtFrontBackFirstLast(void);
    void testAccessOperator(void);
    void testSetAt(void);
    void testReplace(void);
    void testIterate(void);
    void testIterateReverse(void);
    void testRawData(void);
    void testView(void);
    void testCompare(void);
    void testSearch(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkArrayTest.h ends here
