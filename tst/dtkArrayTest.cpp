// Version: $Id$
//
//

// Commentary: see EOF for Credits
//
//

// Change Log:
//
//

// Code:

#include "dtkArrayTest.h"

#include <dtkContainers>

#include <QtCore>
#include <QVector3D>

// /////////////////////////////////////////////////////////////////
// Debug for QString array type
// /////////////////////////////////////////////////////////////////

std::ostream& operator << (std::ostream& out, const dtk::ArrayBase<QString>& x)
{
    typedef typename dtk::ArrayBase<QString>::size_type size_type;

    out << "Array of size " << std::to_string(x.size()) << " :" << std::endl;

    out << "[ ";
    for (size_type i = 0; i != x.size(); ++i) {
        if (i)
            out << ", ";

        out << qPrintable(x[i]);
    }
    out << " ]";

    return out;
}

struct Toto
{
    Toto() : m_value(-1), m_mode("Default") {}
    Toto(int value, std::string mode) : m_value(value), m_mode(mode) {}
    Toto(const Toto& other) : m_value(std::move(other.m_value)), m_mode("Copied") {}
    Toto(Toto&& other) : m_value(std::move(other.m_value)), m_mode("Moved") {}

    Toto& operator = (const Toto& o) = default;

    int value(void) const { return m_value; }
    std::string mode(void) const { return m_mode; }

private:
    int m_value;
    std::string m_mode;
};

std::ostream& operator << (std::ostream& out, const Toto& cv)
{
    out << "(Toto : value = " << std::to_string(cv.value()) << ", mode = " << cv.mode() << ")" ;
    return out;
}

// /////////////////////////////////////////////////////////////////
// Helper classes
// /////////////////////////////////////////////////////////////////

// Exception type that is thrown by ComplexValue.
class ComplexValueException
{
public:
    ComplexValueException(int value, bool inCtor)
        : m_value(value), m_inCtor(inCtor) {}

    int value() const { return m_value; }
    bool inConstructor() const { return m_inCtor; }

private:
    int m_value;
    bool m_inCtor;
};

// Complex type that helps the tests determine if QArray is calling
// constructors, destructors, and copy constructors in the right places.
class ComplexValue
{
public:
    enum Mode
    {
        Default,
        Init,
        Copy,
        CopiedAgain,
        Assign,
        ThrowInCtor,
        ThrowOnCopy
    };

    static int destroyCount;

    ComplexValue() : m_value(-1), m_mode(Default) {}
    ComplexValue(int value) : m_value(value), m_mode(Init) {}
#ifndef QT_NO_EXCEPTIONS
    ComplexValue(int value, Mode mode) : m_value(value), m_mode(mode)
    {
        if (mode == ThrowInCtor)
            throw new ComplexValueException(value, true);
    }
#endif
    ComplexValue(const ComplexValue& other)
        : m_value(other.m_value)
    {
        if (other.m_mode == Copy || other.m_mode == CopiedAgain)
            m_mode = CopiedAgain;
#ifndef QT_NO_EXCEPTIONS
        else if (other.m_mode == ThrowOnCopy)
            throw new ComplexValueException(other.m_value, false);
#endif
        else
            m_mode = Copy;
    }
    ~ComplexValue() { ++destroyCount; }

    ComplexValue& operator=(const ComplexValue& other)
    {
#ifndef QT_NO_EXCEPTIONS
        if (other.m_mode == ThrowOnCopy)
            throw new ComplexValueException(other.m_value, false);
#endif
        m_value = other.m_value;
        m_mode = Assign;
        return *this;
    }

    int value() const { return m_value; }
    Mode mode() const { return m_mode; }

    bool operator==(const ComplexValue& other) const
        { return m_value == other.m_value; }
    bool operator==(int other) const
        { return m_value == other; }
    bool operator!=(const ComplexValue& other) const
        { return m_value != other.m_value; }
    bool operator!=(int other) const
        { return m_value != other; }

private:
    int m_value;
    Mode m_mode;
};

int ComplexValue::destroyCount = 0;

std::ostream& operator << (std::ostream& out, const ComplexValue& cv)
{
    out << "(Complex Value : value = " << std::to_string(cv.value()) << ", mode = " << std::to_string(cv.mode()) << ")" ;
    return out;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkArrayTestCase::dtkArrayTestCase(void)
{
}

dtkArrayTestCase::~dtkArrayTestCase(void)
{

}

void dtkArrayTestCase::initTestCase(void)
{

}

void dtkArrayTestCase::init(void)
{

}

void dtkArrayTestCase::cleanup(void)
{

}

void dtkArrayTestCase::cleanupTestCase(void)
{

}

void dtkArrayTestCase::testCreateBasics(void)
{
    {
        // Check the basic properties for void array.
        dtk::Array<double> array;
        QVERIFY(array.empty());
        QCOMPARE(array.count(), 0UL);
        QCOMPARE(array.size(), 0UL);
        QCOMPARE(array.length(), 0UL);
        QCOMPARE(array.capacity(), 0UL);
        QVERIFY(!array.data());
    }

    {
        // Check the basic properties for initialized array
        dtk::Array<double> array(111);
        QVERIFY(!array.empty());
        QCOMPARE(array.count(), 111UL);
        QCOMPARE(array.size(), 111UL);
        QCOMPARE(array.length(), 111UL);
        QCOMPARE(array.capacity(), 111UL);
        QVERIFY(array.data());
    }

    {
        // Create another array that is filled with an initial value.
        dtk::Array<QVector3D> array(100, QVector3D(1.0f, 2.0f, 3.0f));
        QVERIFY(!array.empty());
        QCOMPARE(array.count(), 100UL);
        QCOMPARE(array.size(), 100UL);
        QVERIFY(array.capacity() >= 100UL);
        QVERIFY(array.data());
        for (qlonglong index = 0; index < 100; ++index) {
            QVERIFY(array.at(index) == QVector3D(1.0f, 2.0f, 3.0f));
        }
    }

    {
        // Create an array of strings.
        dtk::Array<QString> array;
        QCOMPARE(array.size(), 0UL);
        array.append(QLatin1String("foo"));
        array.append(QLatin1String("bar"));
        array.append(array[0]);
        QCOMPARE(array[0], QLatin1String("foo"));
        QCOMPARE(array[1], QLatin1String("bar"));
        QCOMPARE(array[2], QLatin1String("foo"));

        // Copy ctor
        dtk::Array<QString> b(array);

    }

    {
        ComplexValue::destroyCount = 0;
        // Create an array of complex values and check that the
        // copy constructors were called correctly.  Also check that
        // the destructors are called when the array is destroyed.
        dtk::Array<ComplexValue> array; array.reserve(3);
        array.append(ComplexValue(1));
        array.append(ComplexValue(2));
        array.append(ComplexValue(3));
        QCOMPARE(ComplexValue::destroyCount, 3); // Destruction of temporaries.
        QCOMPARE(array.size(), 3UL);
        QVERIFY(array[0].value() == 1);
        QVERIFY(array[0].mode()  == ComplexValue::Copy);
        QVERIFY(array[1].value() == 2);
        QVERIFY(array[1].mode()  == ComplexValue::Copy);
        QVERIFY(array[2].value() == 3);
        QVERIFY(array[2].mode()  == ComplexValue::Copy);
    }
    QCOMPARE(ComplexValue::destroyCount, 6);
}

void dtkArrayTestCase::testCreateListAndBufferAndRange(void)
{
    double b[7] = {0., 1., 2., 3., 4., 5., 6.};

    // From buffer
    {
        dtk::Array<double> array(b, 7);
        for (int i = 0; i < 7; ++i) {
            QCOMPARE(array[i], b[i]);
        }
    }

    // From std::initializer_list
    {
        dtk::Array<double> array({0., 1., 2., 3., 4., 5., 6.});
        for (int i = 0; i < 7; ++i) {
            QCOMPARE(array[i], b[i]);
        }
    }

    // From range
    {
        int from = 2;
        int   to = 6;
        dtk::Array<double> array(&b[from], &b[to]);
        QVERIFY(array.size() == (std::size_t)(to-from));
        for (int i = 0; i < (to-from); ++i) {
            QCOMPARE(array[i], b[i+from]);
        }
    }
}

void dtkArrayTestCase::testCopyAndAssignement(void)
{
    std::size_t capacity = 11;
    // Construction and assignement
    dtk::Array<double> a_source(capacity, 42.5f);
    dtk::Array<double> a_target1(a_source);
    dtk::Array<double> a_target2; a_target2 = a_target1;
    dtk::Array<double> a_target3(a_source);
    dtk::Array<double> a_target4(std::move(a_target3));

    QCOMPARE(a_source.size(), capacity);
    QCOMPARE(a_source.size(), a_target1.size());
    QCOMPARE(a_source.size(), a_target2.size());
    QCOMPARE(a_source.size(), a_target3.size());
    QCOMPARE(a_source.size(), a_target4.size());

    for (std::size_t index = 0; index < capacity; ++index) {
        QCOMPARE(a_source.at(index), a_target1.at(index));
        QCOMPARE(a_source.at(index), a_target2.at(index));
        QCOMPARE(a_source.at(index), a_target4.at(index));
    }

    a_target2 = a_target3;
    a_target4.clear();
    a_target4 = std::move(a_target3);

    QCOMPARE(a_target2.size(), a_target4.size());
    QCOMPARE(a_target3.size(), 0UL);
    for (std::size_t index = 0; index < a_target4.size(); ++index) {
        QCOMPARE(a_target2[index], a_target4[index]);
    }

    // Exercise the various conditionals in operator=().
    dtk::Array<double> array, array2, array3;

    array2.append(1.0);
    array2.append(7.0);
    array = array2;
    QCOMPARE(array.count(), 2UL);
    QCOMPARE(array.at(0), double(1.0));
    QCOMPARE(array.at(1), double(7.0));

    array = array3;
    QCOMPARE(array.count(), 0UL);

    array = array2;
    QCOMPARE(array.count(), 2UL);
    QCOMPARE(array.at(0), double(1.0));
    QCOMPARE(array.at(1), double(7.0));

    dtk::Array<double> array4(array2);
    QCOMPARE(array4.count(), 2UL);
    QCOMPARE(array4.at(0), double(1.0));
    QCOMPARE(array4.at(1), double(7.0));

    dtk::Array<double> array5, array6;
    for (int index = 0; index < 32; ++index)
        array5.append(2.0);
    QCOMPARE(array5.count(), 32UL);
    array6.append(1.0);
    array5 = array6;
    QCOMPARE(array5.count(), 1UL);
    QCOMPARE(array5.at(0), double(1.0));

    array5.clear();
    QCOMPARE(array5.count(), 0UL);
    QCOMPARE(array6.count(), 1UL);
    QCOMPARE(array6.at(0), double(1.0));
    array6.clear();
    QCOMPARE(array6.count(), 0UL);
}

void dtkArrayTestCase::testSwap(void)
{
    {
        dtk::Array<double> a0(111, 3.14159);
        dtk::Array<double> a1( 57, 1.414);

        double *ptr0 = a0.data();
        double *ptr1 = a1.data();

        a0.swap(a1);

        QCOMPARE(a0.data(), ptr1);
        QCOMPARE(a0.size(), 57UL);
        for (int i = 0; i < 57; ++i) {
            QCOMPARE(a0[i], 1.414);
        }

        QCOMPARE(a1.data(), ptr0);
        QCOMPARE(a1.size(), 111UL);
        for (int i = 0; i < 111; ++i) {
            QCOMPARE(a1[i], 3.14159);
        }

        dtk::Array<double> a2(std::move(a0));
        QCOMPARE(a2.data(), ptr1);
        QCOMPARE(a2.size(),  57UL);
        for (int i = 0; i < 57; ++i) {
            QCOMPARE(a2[i], 1.414);
        }

        a2 = std::move(a1);
        QCOMPARE(a2.data(), ptr0);
        QCOMPARE(a2.size(), 111UL);
        for (int i = 0; i < 111; ++i) {
            QCOMPARE(a2[i], 3.14159);
        }
    }
}

void dtkArrayTestCase::testClear(void)
{
    {
        dtk::Array<double> a(111, 3.14159);
        a.clear();
        QCOMPARE(a.size(), 0UL);
        QCOMPARE(a.capacity(), 111UL);
    }
}

void dtkArrayTestCase::testResize(void)
{
    std::size_t capacity = 11;

    dtk::Array<int> array;

    array.resize(0);
    QCOMPARE(array.count(), 0UL);

    array.resize(capacity);
    QCOMPARE(array.count(), capacity);
    for (std::size_t index = 0; index < capacity; ++index)
        QCOMPARE(array[index], 0);
    array.append(1);

    array.resize(100);

    QCOMPARE(array.count(), 100UL);
    QVERIFY(array.capacity() >= 100UL);
    std::size_t cap = array.capacity();
    for (std::size_t index = 0; index < 100; ++index){
        if (index != capacity)
            QCOMPARE(array[index], 0);
        else
            QCOMPARE(array[index], 1);
    }

    array.resize(50);
    QCOMPARE(array.count(), 50UL);
    QVERIFY(array.capacity() == cap); // Shouldn't change the capacity.
    for (std::size_t index = 0; index < 50; ++index){
        if (index != capacity)
            QCOMPARE(array[index], 0);
        else
            QCOMPARE(array[index], 1);
    }

    dtk::Array<int> array2(array);
    array.resize(40);
    QCOMPARE(array.count(), 40UL);
    QCOMPARE(array2.count(), 50UL);
    QCOMPARE(array2.capacity(), 50UL);

    array2.resize(20);
    QCOMPARE(array2.count(), 20UL);
    QCOMPARE(array2.capacity(), 50UL);

    for (std::size_t index = 0; index < 40; ++index){
        if (index != capacity)
            QCOMPARE(array[index], 0);
        else
            QCOMPARE(array[index], 1);
    }
    for (std::size_t index = 0; index < 20; ++index){
        if (index != capacity)
            QCOMPARE(array2[index], 0);
        else
            QCOMPARE(array2[index], 1);
    }

    // Check that resizing to zero keeps the same memory storage.
    int *d = array.data();
    std::size_t old_capacity = array.capacity();
    array.resize(0);
    QVERIFY(array.data() == d);
    QVERIFY(array.capacity() == old_capacity);

    // Calling clear does the same
    d = array2.data();
    old_capacity = array2.capacity();
    array2.clear();
    QVERIFY(array2.data() == d);
    QVERIFY(array2.capacity() == old_capacity);

    // Resize with a default value
    dtk::Array<double> a(121, 3.14159);
    QCOMPARE(a.length(), 121UL);
    for(std::size_t i = 0; i < array.size(); ++i){
        QCOMPARE(a[i], 3.14159);
    }
}

void dtkArrayTestCase::testReserve(void)
{
    dtk::Array<double> array;
    array.reserve(1000);
    QVERIFY(array.empty());
    QVERIFY(array.capacity() >= 1000UL);

    // Append elements and check for reallocation.
    const double *d = array.constData();
    for (std::size_t index = 0; index < 1000; ++index) {
        array.append(double(index));
        QVERIFY(array.constData() == d);
    }

    // Reserving less doesn't change the capacity, or the count.
    array.reserve(50);
    QVERIFY(array.capacity() >= 1000UL);
    QCOMPARE(array.count(), 1000UL);
}

void dtkArrayTestCase::testSqueezeAndShrink(void)
{
    dtk::Array<double> array;
    array.reserve(100);
    QVERIFY(array.empty());
    QVERIFY(array.capacity() >= 100UL);

    for (std::size_t index = 0; index < 100; ++index)
        array.append(double(index));

    array.reserve(400);
    QVERIFY(array.capacity() >= 400UL);

    array.squeeze();
    QCOMPARE(array.capacity(), 100UL);
    QCOMPARE(array.count(), 100UL);

    // Test squeezing after appending
    dtk::Array<double> array2;
    array2.append(1.0);
    array2.append(2.0);
    array2.append(3.0);
    array2.squeeze();
    QCOMPARE(array2.capacity(), 3UL);
    QCOMPARE(array2.count(), 3UL);

    // Test copy-on-write during squeezing.
    dtk::Array<double> array3(array);
    array3.shrink_to_fit();
    QCOMPARE(array3.count(), 100UL);
    QCOMPARE(array.count(), 100UL);

    // Clear and shrink then check that the array reverts to preallocation.has zero capacity
    array.clear();
    array.shrink_to_fit();
    QCOMPARE(array.size(), 0UL);
    QCOMPARE(array.capacity(), 0UL);
}

void dtkArrayTestCase::testFill(void)
{
    dtk::Array<double> array;
    array.fill(1.0);
    QCOMPARE(array.size(), 0UL);

    array.resize(100);
    array.fill(1.0);
    QCOMPARE(array.size(), 100UL);
    for (std::size_t index = 0; index < 100; ++index)
        QCOMPARE(array.at(index), 1.0);

    array.fill(2.0);
    QCOMPARE(array.size(), 100UL);
    for (std::size_t index = 0; index < 100; ++index)
        QCOMPARE(array.at(index), 2.0);

    array.resize(20);
    array.fill(3.0);
    QCOMPARE(array.size(), 20UL);
    for (std::size_t index = 0; index < 20; ++index)
        QCOMPARE(array.at(index), 3.0);
}

void dtkArrayTestCase::testInsertValue(void)
{
    std::size_t N = 10;
    double pi = 3.14159;
    double sq = 1.414;
    dtk::Array<double> array(N, pi);

    // insert middle
    std::size_t mid = array.size() / 2;
    array.insert(mid, sq);
    ++N;
    QCOMPARE(array.size(), N);
    QCOMPARE(array[mid], sq);

    // insert front
    array.insert(array.cbegin(), sq);
    ++N;
    QCOMPARE(array.size(), N);
    QCOMPARE(array.first(), sq);

    // insert back
    array.insert(array.cend(), sq);
    ++N;
    QCOMPARE(array.size(), N);
    QCOMPARE(array.last(), sq);

    // insert rvalue
    mid = array.size() / 2;
    array.insert(array.cbegin() + mid, std::move(sq));
    ++N;
    QCOMPARE(array.size(), N);
    QCOMPARE(array[mid], sq);

    // insert N times the value
    mid = array.size() / 2;
    auto it = array.insert(array.cbegin() + mid, N, sq);
    for (std::size_t i = 0; i < N; ++i, ++it) {
        QCOMPARE(*it, sq);
    }

    // insert without resizing
    array.reserve(4*N);
    QCOMPARE(array.capacity(), 4*N);
    mid = array.size() / 2;
    array.insert(mid, N, sq);
    it =  array.begin() + mid;
    for (std::size_t i = 0; i < N; ++i, ++it) {
        QCOMPARE(*it, sq);
    }
    QCOMPARE(array.capacity(), 4*N);
}

void dtkArrayTestCase::testInsertRange(void)
{
    double b[7] = {0., 1., 2., 3., 4., 5., 6.};

    std::size_t N = 10;
    double pi = 3.14159;
    dtk::Array<double> a(N, pi);

    // Insert a subset of the buffer
    std::size_t mid = a.size() / 2;
    std::size_t from = 2;
    std::size_t to = 5;
    auto it = a.insert(a.cbegin() + mid, &b[from], &b[to]);
    for (std::size_t i = from; i < to; ++i, ++it) {
        QCOMPARE(*it, b[i]);
    }

    // Insert a list
    mid = a.size() / 2;
    from = 0;
    to = 7;
    it = a.insert(a.cbegin() + mid, {0., 1., 2., 3., 4., 5., 6.});
    for (std::size_t i = from; i < to; ++i, ++it) {
        QCOMPARE(*it, b[i]);
    }
}

void dtkArrayTestCase::testInsertComplexType(void)
{
    // Repeat the tests with QString
    dtk::Array<QString> array2;
    for (int index = 0; index < 10; ++index)
        array2.append(QString::number(index));

    array2.prepend(QString::number(-1));
    QCOMPARE(array2.size(), 11UL);
    for (int index = 0; index < (int)array2.size(); ++index)
        QCOMPARE(array2[index], QString::number(index - 1));

    array2.insert(array2.size(), QString::number(10));
    QCOMPARE(array2.size(), 12UL);
    for (int index = 0; index < (int)array2.size(); ++index)
        QCOMPARE(array2[index], QString::number(index - 1));

    array2.insert(1, QString::number(5));
    QCOMPARE(array2.size(), 13UL);
    QCOMPARE(array2[0], QString::number(-1));
    QCOMPARE(array2[1], QString::number(5));
    QCOMPARE(array2[2], QString::number(0));
    QCOMPARE(array2[12], QString::number(10));

    array2.insert(10, 4, QString::number(3));
    QCOMPARE(array2.size(), 17UL);
    QCOMPARE(array2[9], QString::number(7));
    QCOMPARE(array2[10], QString::number(3));
    QCOMPARE(array2[11], QString::number(3));
    QCOMPARE(array2[12], QString::number(3));
    QCOMPARE(array2[13], QString::number(3));
    QCOMPARE(array2[14], QString::number(8));
    QCOMPARE(array2[15], QString::number(9));
    QCOMPARE(array2[16], QString::number(10));

    // Repeat the tests with ComplexValue
    ComplexValue::destroyCount = 0;
    dtk::Array<ComplexValue> array3; array3.reserve(11);
    for (std::size_t index = 0; index < 10; ++index)
        array3.append(ComplexValue(index));

    array3.prepend(ComplexValue(-1));
    ComplexValue::destroyCount = 0;
    QCOMPARE(array3.size(), 11UL);
    QVERIFY(array3[0] == - 1);
    for (std::size_t index = 0; index < array3.size() - 1; ++index) {
        QVERIFY(array3[index] == (index - 1));
        QCOMPARE((int)array3[index].mode(), (int)ComplexValue::Assign);
    }
    QCOMPARE((int)array3.last().mode(), (int)ComplexValue::CopiedAgain);
    QCOMPARE(ComplexValue::destroyCount, 0);

    array3.insert(array3.size(), ComplexValue(10));
    QCOMPARE(array3.size(), 12UL);
    for (std::size_t index = 0; index < array3.size(); ++index)
        QVERIFY(array3[index] == (index - 1));

    array3.insert(1, ComplexValue(5));
    QCOMPARE(array3.size(), 13UL);
    QVERIFY(array3[0] == -1);
    QVERIFY(array3[1] == 5);
    QVERIFY(array3[2] == 0);
    QVERIFY(array3[12] == 10);

    array3.insert(10, 4, ComplexValue(3));
    QCOMPARE(array3.size(), 17UL);
    QVERIFY(array3[9] == 7);
    QVERIFY(array3[10] == 3);
    QVERIFY(array3[11] == 3);
    QVERIFY(array3[12] == 3);
    QVERIFY(array3[13] == 3);
    QVERIFY(array3[14] == 8);
    QVERIFY(array3[15] == 9);
    QVERIFY(array3[16] == 10);
}

void dtkArrayTestCase::testAppend(void)
{
    std::size_t ExpectedMinCapacity = 8;

    // Double array
    dtk::Array<double> array;
    std::size_t index;

    // Appending up to the minimum capacity should not cause a realloc.
    const double *d = array.constData();
    for (index = 0; index < ExpectedMinCapacity; ++index) {
        array.append(double(index));
        QCOMPARE(array[index], double(index));
    }

    // Check that the array contains the values we expected.
    QCOMPARE(array.count(), ExpectedMinCapacity);
    QCOMPARE(array.capacity(), ExpectedMinCapacity);
    for (index = 0; index < ExpectedMinCapacity; ++index) {
        QCOMPARE(array[index], double(index));
    }

    // Append 1 more item and check for realloc.
    array += double(1000.0);
    QCOMPARE(array.count(), ExpectedMinCapacity + 1);
    QVERIFY(array.capacity() > ExpectedMinCapacity);
    QVERIFY(array.capacity() >= array.count());
    QCOMPARE(array.size(), array.count());

    // Check that the array still contains the values we expected.
    for (index = 0; index < ExpectedMinCapacity; ++index) {
        QCOMPARE(array[index], double(index));
    }
    QCOMPARE(array[ExpectedMinCapacity], 1000.0);

    // Append a large number of values to test constant reallocation.
    for (index = 0; index < 1000; ++index)
        array.append(double(index));
    QCOMPARE(array.count(), ExpectedMinCapacity + 1 + 1000);
    QCOMPARE(array.size(), array.count());

    // Make two copies of the array.
    dtk::Array<double> array2(array);
    QCOMPARE(array2.count(), ExpectedMinCapacity + 1 + 1000);
    QCOMPARE(array2.size(), array2.count());
    QVERIFY(!array2.empty());
    QVERIFY(array2.capacity() >= array2.size());
    dtk::Array<double> array3;
    array3 = array;
    QCOMPARE(array3.count(), ExpectedMinCapacity + 1 + 1000);
    QCOMPARE(array3.size(), array3.count());
    QVERIFY(!array3.empty());
    QVERIFY(array3.capacity() >= array3.size());

    d = array2.constData();

    // Add another item to the original and check that the copy is unchanged.
    array << double(1500.0);
    QCOMPARE(array.count(), ExpectedMinCapacity + 1 + 1000 + 1);
    QCOMPARE(array2.count(), ExpectedMinCapacity + 1 + 1000);
    QCOMPARE(array3.count(), ExpectedMinCapacity + 1 + 1000);

    // Check that pointers
    QVERIFY(array.constData() != array2.constData());
    QVERIFY(array.constData() != array3.constData());
    QVERIFY(array2.constData() != array3.constData());
    QVERIFY(array2.constData() == d);

    // Check that the original and the copy contain the right values.
    for (index = 0; index < ExpectedMinCapacity; ++index) {
        QCOMPARE(array.at(index), double(index));
        QCOMPARE(array2.at(index), double(index));
        QCOMPARE(array3.at(index), double(index));
    }
    QCOMPARE(array.at(ExpectedMinCapacity), 1000.0);
    QCOMPARE(array2.at(ExpectedMinCapacity), 1000.0);
    QCOMPARE(array3.at(ExpectedMinCapacity), 1000.0);
    for (index = 0; index < 1000; ++index) {
        QCOMPARE(array.at(index + ExpectedMinCapacity + 1), double(index));
    }
    QCOMPARE(array[ExpectedMinCapacity + 1000 + 1], 1500.0);

    // Create a large array of strings.
    dtk::Array<QString> array4;
    for (index = 0; index < 1000; ++index)
        array4.append(QString::number(index));
    QCOMPARE(array4.size(), 1000UL);
    for (index = 0; index < 1000; ++index)
        QVERIFY(array4[index] == QString::number(index));

    // Make a copy of the array of strings and then force a detach.
    dtk::Array<QString> array5(array4);
    QCOMPARE(array4.size(), 1000UL);
    QCOMPARE(array5.size(), 1000UL);
    for (index = 0; index < 1000; ++index) {
        QVERIFY(array4[index] == QString::number(index));
        QVERIFY(array5[index] == QString::number(index));
    }
    array5.append(QString::number(1000));
    QCOMPARE(array4.size(), 1000UL);
    QCOMPARE(array5.size(), 1001UL);
    for (index = 0; index < 1000; ++index) {
        QVERIFY(array4[index] == QString::number(index));
        QVERIFY(array5[index] == QString::number(index));
    }
    QVERIFY(array5[1000] == QString::number(1000));

    // Create an array of complex values and force one realloc
    // to test that copy constructors and destructors are called
    dtk::Array<ComplexValue> array6; array6.reserve(ExpectedMinCapacity);
    ComplexValue::destroyCount = 0;
    for (index = 0; index < ExpectedMinCapacity; ++index)
        array6.append(ComplexValue(index));
    QCOMPARE((std::size_t)ComplexValue::destroyCount, ExpectedMinCapacity);
    ComplexValue::destroyCount = 0;
    array6.append(ComplexValue(ExpectedMinCapacity));
    QCOMPARE((std::size_t)ComplexValue::destroyCount, ExpectedMinCapacity + 1);
    for (index = 0; index < (ExpectedMinCapacity + 1); ++index) {
        QCOMPARE(array6[index].value(), (int)index);
        // The last element should be Copy, but all others are CopiedAgain.
        if (index == ExpectedMinCapacity)
            QVERIFY(array6[index].mode() == ComplexValue::Copy);
        else
            QVERIFY(array6[index].mode() == ComplexValue::CopiedAgain);
    }

    // Force another realloc to test copies.
    std::size_t capacity = array6.capacity();
    for (std::size_t index = array6.size(); index < capacity; ++index)
        array6.append(ComplexValue(index));
    ComplexValue::destroyCount = 0;
    array6.append(ComplexValue(capacity));
    QCOMPARE(ComplexValue::destroyCount, (int)capacity + 1);
    for (index = 0; index < (capacity + 1); ++index) {
        QCOMPARE(array6[index].value(), (int)index);
        // The last element should be Copy, but all others are CopiedAgain.
        if (index == capacity)
            QVERIFY(array6[index].mode() == ComplexValue::Copy);
        else
            QVERIFY(array6[index].mode() == ComplexValue::CopiedAgain);
    }

    // Make a copy of array6.
    std::size_t size = array6.size();
    dtk::Array<ComplexValue> array7(array6);
    QCOMPARE(array6.size(), size);
    QCOMPARE(array7.size(), size);
    for (index = 0; index < size; ++index) {
        QVERIFY(array6[index].value() == (int)index);
        QVERIFY(array7[index].value() == (int)index);
    }
    array7.append(ComplexValue(size));
    QCOMPARE(array6.size(), size);
    QCOMPARE(array7.size(), size + 1);
    for (index = 0; index < size; ++index) {
        QVERIFY(array6[index].value() == (int)index);
        QVERIFY(array7[index].value() == (int)index);
    }
    QVERIFY(array7[size].value() == (int)size);

    // Make another copy using operator=.
    dtk::Array<ComplexValue> array8;
    QCOMPARE(array8.size(), 0UL);
    array8 = array6;
    QCOMPARE(array6.size(), size);
    QCOMPARE(array8.size(), size);
    for (index = 0; index < size; ++index) {
        QVERIFY(array6[index].value() == (int)index);
        QVERIFY(array8[index].value() == (int)index);
    }
    array8.append(ComplexValue(size));
    QCOMPARE(array6.size(), size);
    QCOMPARE(array8.size(), size + 1);
    for (index = 0; index < size; ++index) {
        QVERIFY(array6[index].value() == (int)index);
        QVERIFY(array8[index].value() == (int)index);
    }
    QVERIFY(array8[size].value() == (int)size);

    // Copy the same object over itself.
    dtk::Array<ComplexValue> array9(array8);
    QVERIFY(array9.constData() != array8.constData());
    for (index = 0; index < array8.size(); ++index)
        QCOMPARE((*((const dtk::Array<ComplexValue> *)&array9))[index],
                 array8.at(index));
    array9 = array8;
    QVERIFY(array9.constData() != array8.constData());
    for (index = 0; index < array8.size(); ++index)
        QCOMPARE(array9.at(index), array8.at(index));
}

void dtkArrayTestCase::testAppendArray(void)
{
    dtk::Array<double> array;
    dtk::Array<double> array2;
    std::size_t index;

    for (index = 0; index < 1000; ++index) {
        array.append(index);
        array2.append(1000 - index);
    }

    array.append(array2);
    QCOMPARE(array.count(), 2000UL);
    for (index = 0; index < 1000; ++index) {
        QCOMPARE(array[index], double(index));
        QCOMPARE(array[index + 1000], double(1000 - index));
    }

    array2 << array2;
    QCOMPARE(array2.count(), 2000UL);
    for (index = 0; index < 1000; ++index) {
        QCOMPARE(array2[index], double(1000 - index));
        QCOMPARE(array2[index + 1000], double(1000 - index));
    }

    array2 += (dtk::Array<double>());
    QCOMPARE(array2.count(), 2000UL);

    dtk::Array<double> array3; array3.append(array2.constData(), array2.size());
    QCOMPARE(array3.size(), array2.size());
    for (index = 0; index < array2.size(); ++index)
        QCOMPARE(array3.at(index), array2.at(index));
}

void dtkArrayTestCase::testAppendList(void)
{
    auto list   = {0., 1., 2., 3., 4., 5., 6.};
    double b[7] = {0., 1., 2., 3., 4., 5., 6.};

    dtk::Array<double> array;
    array << list;
    QVERIFY(array.size() == 7UL);

    for (std::size_t index = 0; index < 7; ++index) {
        QCOMPARE(array[index], b[index]);
    }
    array.clear();
    QVERIFY(array.size() == 0UL);

    list = {1., 2., 3., 4., 5., 6., -1, -2};
    double c[8] = {1., 2., 3., 4., 5., 6., -1, -2};
    array += list;
    QVERIFY(array.size() == 8UL);

    for (std::size_t index = 0; index < 8; ++index) {
        QCOMPARE(array[index], c[index]);
    }
}

void dtkArrayTestCase::testPushBack(void)
{
    // Create a large array of strings.
    dtk::Array<QString> array1;
    // Push back rvalues
    for (std::size_t i = 0; i < 1000; ++i)
        array1.push_back(QString::number(i));
    QCOMPARE(array1.size(), 1000UL);
    for (std::size_t i = 0; i < 1000; ++i)
        QVERIFY(array1[i] == QString::number(i));

    // Make a copy of the array of strings
    dtk::Array<QString> array2(array1);
    QCOMPARE(array1.size(), 1000UL);
    QCOMPARE(array2.size(), 1000UL);
    for (std::size_t i = 0; i < 1000; ++i) {
        QVERIFY(array1[i] == QString::number(i));
        QVERIFY(array2[i] == QString::number(i));
    }
    QString thousand = QString::number(1000);
    // Push back const ref
    array2.push_back(thousand);
    QCOMPARE(array1.size(), 1000UL);
    QCOMPARE(array2.size(), 1001UL);
    for (std::size_t i = 0; i < 1000; ++i) {
        QVERIFY(array1[i] == QString::number(i));
        QVERIFY(array2[i] == QString::number(i));
    }
    QVERIFY(array2[1000] == QString::number(1000));
}

void dtkArrayTestCase::testEmplaceAndEmplaceBack(void)
{
    {
        dtk::Array<Toto> a; a.reserve(7);
        a.emplace_back(0, "Init");
        a.emplace_back(1, "Init");

        a.push_back(Toto(3, "Init"));
        a.push_back(Toto(4, "Init"));

        auto cit = a.cbegin() + 2;
        a.emplace(cit, 2, "Init");

        std::size_t i = 0;
        for(const Toto& cv : a) {
            QCOMPARE(cv.value(), (int)i++);
        }
        QCOMPARE(a.at(0).mode(), std::string("Init"));
        QCOMPARE(a.at(1).mode(), std::string("Init"));
        QCOMPARE(a.at(2).mode(), std::string("Init"));
        QCOMPARE(a.at(3).mode(), std::string("Moved"));
        QCOMPARE(a.at(4).mode(), std::string("Copied"));
    }

    dtk::Array<ComplexValue> a; a.reserve(7);
    a.emplace_back(0, ComplexValue::Init);
    a.emplace_back(1, ComplexValue::Init);
    a.emplace_back(2, ComplexValue::Init);
    a.emplace_back(3, ComplexValue::Init);
    a.emplace_back(4, ComplexValue::Init);

    QVERIFY(a.size() == 5);

    // Check that emplace with constructor arguments does not perform
    // any copy.
    std::size_t i = 0;
    for(const ComplexValue& cv : a) {
        QCOMPARE(cv.value(), (int)i++);
        QCOMPARE(cv.mode(), ComplexValue::Init);
    }

    // However when using rvalue, a copy is done
    a.emplace_back(ComplexValue(5, ComplexValue::Init));
    QVERIFY(a.size() == 6);
    QCOMPARE(a[5].value(), 5);
    QCOMPARE(a[5].mode(), ComplexValue::Copy);

    // Checks when emplace in the middle
    std::size_t mid = a.size() / 2;
    auto cit = a.cbegin() + mid;
    a.emplace(cit, mid, ComplexValue::Init);
    QVERIFY(a.size() == 7);
    QCOMPARE(a[mid].value(), (int)mid);
    QCOMPARE(a[mid].mode(), ComplexValue::Assign);
    // Until mid position previous values were unchanged
    for (i = 0; i < mid - 1; ++i) {
        QCOMPARE(a[i].mode(), ComplexValue::Init);
    }
    // Last pos is copy constructed from a previous copy (see last emplace_back)
    QCOMPARE(a.last().mode(), ComplexValue::CopiedAgain);
    // For other position [mid, last), values were moved so Assign
    // type is expected
    for (i = mid + 1; i < a.size() - 1; ++i) {
        QCOMPARE(a[i].mode(), ComplexValue::Assign);
    }

    // Emplace back now force reallocation
    a.emplace_back(8, ComplexValue::Init);
    QVERIFY(a.size() == 8);

    // Last value does not come from a copy
    QCOMPARE(a[7].value(), 8);
    QCOMPARE(a[7].mode(), ComplexValue::Init);

    // Except for the two last pos, the new values are copy
    // constructed for the first time.
    for (i = 0; i < a.size() - 2; ++i) {
        QCOMPARE(a[i].mode(), ComplexValue::Copy);
    }
    // The penultimate is copy constructed another time
    QCOMPARE(a[a.size() - 2].mode(), ComplexValue::CopiedAgain);

}

void dtkArrayTestCase::testErase(void)
{
    dtk::Array<double> array;

    QCOMPARE(array.count(), 0UL);

    for (std::size_t index = 0; index < 100; ++index)
        array.append(double(index));

    QCOMPARE(array.count(), 100UL);

    auto pos = array.cbegin() + 10;
    array.erase(pos, pos + 20);
    QCOMPARE(array.count(), 80UL);
    for (std::size_t index = 0; index < 80; ++index) {
        if (index < 10)
            QCOMPARE(array[index], double(index));
        else
            QCOMPARE(array[index], double(index + 20));
    }

    dtk::Array<double> array2(array);
    pos = array2.cbegin();
    array2.erase(pos, pos + 10);
    pos += 60;
    array2.erase(pos, pos + 10);
    QCOMPARE(array2.count(), 60UL);
    for (std::size_t index = 0; index < 60; ++index) {
        QCOMPARE(array2[index], double(index + 30));
    }

    pos = array.cbegin();
    array.erase(pos, pos + 10);
    QCOMPARE(array.count(), 70UL);
    for (std::size_t index = 0; index < 70; ++index) {
        QCOMPARE(array[index], double(index + 30));
    }

    // Remove everything
    pos = array.cbegin();
    array.erase(pos, pos + array.size());
    QCOMPARE(array.count(), 0UL);

    for (std::size_t index = 0; index < 100; ++index)
        array.append(double(index));
    array.erase(array.end() - 1);
    QCOMPARE(array.size(), 99UL);
}

void dtkArrayTestCase::testPopBack(void)
{
    std::size_t N = 1000;

    // Create a large array of strings.
    dtk::Array<QString> array;
    array.reserve(N);
    // Push back rvalues
    for (std::size_t i = 0; i < N; ++i)
        array.push_back(QString::number(i));
    QCOMPARE(array.size(), N);
    for (std::size_t i = 0; i < N; ++i)
        QVERIFY(array[i] == QString::number(i));

    //
    for (std::size_t i = 0; i < N; ++i) {
        array.pop_back();
        QCOMPARE(array.size(), (N-i-1));
    }

    QCOMPARE(array.size(), 0UL);
}

void dtkArrayTestCase::testRemove(void)
{
    dtk::Array<double> array;

    QCOMPARE(array.count(), 0UL);

    for (std::size_t index = 0; index < 100; ++index)
        array.append(double(index));

    QCOMPARE(array.count(), 100UL);

    array.remove(10, 20);
    QCOMPARE(array.count(), 80UL);
    for (std::size_t index = 0; index < 80; ++index) {
        if (index < 10)
            QCOMPARE(array[index], double(index));
        else
            QCOMPARE(array[index], double(index + 20));
    }

    dtk::Array<double> array2(array);
    array2.remove(0, 10);
    array2.remove(60, 10);
    QCOMPARE(array2.count(), 60UL);
    for (std::size_t index = 0; index < 60; ++index) {
        QCOMPARE(array2[index], double(index + 30));
    }

    array.remove(0, 10);
    QCOMPARE(array.count(), 70UL);
    for (std::size_t index = 0; index < 70; ++index) {
        QCOMPARE(array[index], double(index + 30));
    }

    // Remove everything
    array.remove(0, array.size());
    QCOMPARE(array.count(), 0UL);

    for (std::size_t index = 0; index < 100; ++index)
        array.append(double(index));
    array.removeLast();
    QCOMPARE(array.size(), 99UL);
    array.removeFirst();
    QCOMPARE(array.size(), 98UL);
}

void dtkArrayTestCase::testTake(void)
{
    dtk::Array<double> array;

    QCOMPARE(array.count(), 0UL);

    for (std::size_t index = 0; index < 100; ++index)
        array.append(double(index));

    QCOMPARE(array.count(), 100UL);

    double val = array.takeAt(10);
    QCOMPARE(val, (double)(10));
    QVERIFY(array.size() == 99UL);

    val = array.takeFirst();
    QCOMPARE(val, (double)(0));
    val = array.takeLast();
    QCOMPARE(val, (double)(99));
    QVERIFY(array.size() == 97UL);
}

void dtkArrayTestCase::testAtFrontBackFirstLast(void)
{
    ComplexValue::destroyCount = 0;
    dtk::Array<ComplexValue> a;
    for (std::size_t index = 0; index < 10; ++index)
        a.append(ComplexValue(index));

    a.prepend(ComplexValue(-1));

    QCOMPARE(a.at(0), ComplexValue(-1));
    QCOMPARE(a.at(0), a.front());
    QCOMPARE(a.first(), a.front());

    for (std::size_t index = 1; index < 10; ++index)
        QCOMPARE(a.at(index), ComplexValue(index-1));

    QCOMPARE(a.at(10), ComplexValue(9));
    QCOMPARE(a.at(10), a.back());
    QCOMPARE(a.last(), a.back());
}

void dtkArrayTestCase::testAccessOperator(void)
{
    std::size_t N = 1000;

    // Create a large array of strings.
    dtk::Array<QString> array(N);
    for (std::size_t i = 0; i < N; ++i)
        QCOMPARE(array[i], QString());
    for (std::size_t i = 0; i < N; ++i)
        array[i] = QString::number(i);
    for (std::size_t i = 0; i < N; ++i)
        QCOMPARE(array[i], QString::number(i));
}

void dtkArrayTestCase::testSetAt(void)
{
    std::size_t index;

    dtk::Array<double> array;
    array.append(1.0);
    QCOMPARE(array[0], 1.0);

    array[0] = 6.0;
    QCOMPARE(array[0], 6.0);

    dtk::Array<double> array2;
    for (index = 0; index < 1000; ++index)
        array2.append(index);
    for (index = 0; index < 1000; ++index)
        array2[index] = index + 1000;
    for (index = 0; index < 1000; ++index)
        QCOMPARE(array2[index], double(index + 1000));
    for (index = 0; index < 1000; ++index)
        array2.setAt(index, double(-index));
    for (index = 0; index < 1000; ++index)
        QCOMPARE(array2[index], double(-index));
}

void dtkArrayTestCase::testReplace(void)
{
    dtk::Array<double> array;
    dtk::Array<double> array2;
    std::size_t index;

    for (index = 0; index < 1000; ++index) {
        array.append(index);
        array2.append(1000 - index);
    }

    array.setAt(500, array2.constData(), 500);
    QCOMPARE(array.count(), 1000UL);
    for (index = 0; index < 1000; ++index) {
        if (index < 500)
            QCOMPARE(array[index], double(index));
        else
            QCOMPARE(array[index], double(1000 - (index - 500)));
    }

    // Replace and extend the array from the middle.
    array.setAt(900, array2.constData(), 100);
    //QCOMPARE(array.count(), 1400UL);
    for (index = 0; index < 1000; ++index) {
        if (index < 500)
            QCOMPARE(array[index], double(index));
        else if (index < 900)
            QCOMPARE(array[index], double(1000 - (index - 500)));
        else
            QCOMPARE(array[index], double(1000 - (index - 900)));
    }

    // Check the bail-out cases when index is negative or count zero.
    array.setAt(900, array2.constData(), 0);
    //array.setAt(-1, array2.constData(), 900);
    QCOMPARE(array.count(), 1000UL);
    for (index = 0; index < 1000; ++index) {
        if (index < 500)
            QCOMPARE(array[index], double(index));
        else if (index < 900)
            QCOMPARE(array[index], double(1000 - (index - 500)));
        else
            QCOMPARE(array[index], double(1000 - (index - 900)));
    }

    // Replace beyond the end of the array.
    static double const extras[] = {0.0, 0.0, 1.0, 2.0, 3.0, 4.0};
    array.setAt(996, extras + 2, 4);
    QCOMPARE(array.count(), 1000UL);
    for (index = 0; index < 1000; ++index) {
        if (index < 500)
            QCOMPARE(array[index], double(index));
        else if (index < 900)
            QCOMPARE(array[index], double(1000 - (index - 500)));
        else if (index < 996)
            QCOMPARE(array[index], double(1000 - (index - 900)));
        else
            QCOMPARE(array[index], extras[index - 994]);
    }

    dtk::Array<ComplexValue> array3;
    dtk::Array<ComplexValue> array4;
    for (index = 0; index < 1000; ++index) {
        array3.append(ComplexValue(index));
        array4.append(ComplexValue(1000 - index));
    }
    array3.setAt(0, array4.constData(), array4.size());
    for (index = 0; index < 1000; ++index)
        QVERIFY(array3[index] == array4[index]);
}

void dtkArrayTestCase::testIterate(void)
{
    dtk::Array<double> array;
    for (std::size_t index = 0; index < 1024; ++index)
        array.append(double(index));

    dtk::Array<double>::iterator it1;
    std::size_t value = 0;
    for (it1 = array.begin(); it1 != array.end(); ++it1)
        QCOMPARE(*it1, double(value++));
    QCOMPARE(value, array.size());

    dtk::Array<double>::const_iterator it2;
    value = 0;
    for (it2 = array.cbegin(); it2 != array.cend(); ++it2)
        QCOMPARE(*it2, double(value++));
    QCOMPARE(value, array.size());

    value = 0;
    for (it1 = array.begin(); it1 != array.end(); ++it1)
        *it1 = double(1024 - value++);
    value = 0;
    for (it2 = array.cbegin(); it2 != array.cend(); ++it2) {
        QCOMPARE(*it2, double(1024 - value));
        QCOMPARE(array[value], double(1024 - value));
        ++value;
    }

    for (std::size_t index = 0; index < 1024; ++index)
        array[index] = double(index);
}

void dtkArrayTestCase::testIterateReverse(void)
{
    dtk::Array<double> array;
    for (std::size_t index = 0; index < 1024; ++index)
        array.append(double(index));

    dtk::Array<double>::reverse_iterator rit1;
    std::size_t value = 1024;
    for (rit1 = array.rbegin(); rit1 != array.rend(); ++rit1)
        QCOMPARE(*rit1, double(--value));
    QCOMPARE(value, 0UL);

    dtk::Array<double>::const_reverse_iterator rit2;
    value = 1024;
    for (rit2 = array.crbegin(); rit2 != array.crend(); ++rit2)
        QCOMPARE(*rit2, double(--value));
    QCOMPARE(value, 0UL);

    value = 1024;
    for (rit1 = array.rbegin(); rit1 != array.rend(); ++rit1)
        *rit1 = double(1024 - (--value));
    value = 1024;
    for (rit2 = array.crbegin(); rit2 != array.crend(); ++rit2) {
        --value;
        QCOMPARE(*rit2, double(1024 - value));
        QCOMPARE(array[value], double(1024 - value));
    }

    for (std::size_t index = 0; index < 1024; ++index)
        array[index] = double(index);
}

void dtkArrayTestCase::testRawData(void)
{
    dtk::Array<double> array;
    double static_contents[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,
                        7.0, 8.0, 9.0, 10.0, 11.0, 12.0};

    array.setStaticRawData(static_contents, 0);
    QCOMPARE(array.size(), 0UL);
    QCOMPARE(array.capacity(), 0UL);

    array.setRawData((double *)nullptr, 0);
    array.append(1.0);
    QCOMPARE(array.size(), 1UL);
    QVERIFY(array.capacity() > 0UL);
    QCOMPARE(array.at(0), 1.0);

    array.setStaticRawData(static_contents, 6);
    QCOMPARE(array.size(), 6UL);
    QCOMPARE(array.capacity(), 6UL);
    for (std::size_t index = 0; index < 6; ++index)
        QCOMPARE(array.at(index), static_contents[index]);
    QVERIFY(array.data() == static_contents);

    double *dynamic_contents = new double[12];

    for (std::size_t i = 0; i < 12; ++i) {
        dynamic_contents[i] = i + 1;
        QCOMPARE(dynamic_contents[i], static_contents[i]);
    }

    // array takes the ownership for the memory management of
    // dynamic_contents
    array.setRawData(dynamic_contents, 12, dtk::BufferManager<double>("dtkMemoryResourceNewAndDelete"));

    QCOMPARE(array.size(), 12UL);
    QVERIFY(array.capacity() >= 12UL);
    for (std::size_t index = 0; index < array.size(); ++index)
        QCOMPARE(array.at(index), dynamic_contents[index]);
    QVERIFY(array.data() == dynamic_contents);

    array[0] = array.last();

    // The memory management ownership means that when setting another
    // raw data, the dynamic_contents is deallocated. It follows that
    // dynamic_contents becomes a dangling pointer.
    array.setStaticRawData(static_contents, 12);

    // deletion of dynamic_contents will now fail
    /*delete dynamic_contents;*/

    QString strings[2] = {QLatin1String("foo"), QLatin1String("bar")};

    dtk::Array<QString> array2;

    array2.setStaticRawData(strings, 2);
    QCOMPARE(array2.size(), 2UL);
    QCOMPARE(array2.capacity(), 2UL);
    QCOMPARE(array2.at(0), QLatin1String("foo"));
    QCOMPARE(array2.at(1), QLatin1String("bar"));
    QVERIFY(array2.constData() == strings);

    array2[1] = QLatin1String("baz");
    QCOMPARE(array2.size(), 2UL);
    QVERIFY(array2.capacity() >= 2UL);
    QCOMPARE(array2.at(0), QLatin1String("foo"));
    QCOMPARE(array2.at(1), QLatin1String("baz"));
    QVERIFY(array2.constData() == strings);
    QCOMPARE(strings[0], QLatin1String("foo"));
    QCOMPARE(strings[1], QLatin1String("baz"));


    QString *dyn_strings = new QString[2];
    dyn_strings[0] = QLatin1String("foo");
    dyn_strings[1] = QLatin1String("bar");

    array2.setStaticRawData(dyn_strings, 2);

    QCOMPARE(strings[0], QLatin1String("foo"));
    QCOMPARE(strings[1], QLatin1String("baz"));
    QCOMPARE(array2.size(), 2UL);
    QCOMPARE(array2.capacity(), 2UL);
    QCOMPARE(array2.at(0), QLatin1String("foo"));
    QCOMPARE(array2.at(1), QLatin1String("bar"));
    QVERIFY(array2.constData() == dyn_strings);

    array2[1] = QLatin1String("baz");
    QCOMPARE(array2.size(), 2UL);
    QVERIFY(array2.capacity() >= 2UL);
    QCOMPARE(array2.at(0), QLatin1String("foo"));
    QCOMPARE(array2.at(1), QLatin1String("baz"));
    QVERIFY(array2.constData() == dyn_strings);
    QCOMPARE(dyn_strings[0], QLatin1String("foo"));
    QCOMPARE(dyn_strings[1], QLatin1String("baz"));

    QString *extr;
    std::size_t extr_size;
    dtk::BufferManager<QString> extr_bm;
    array2.extractRawData(extr, extr_size, extr_bm);

    QCOMPARE(extr[0], QLatin1String("foo"));
    QCOMPARE(extr[1], QLatin1String("baz"));

    QVERIFY(extr_size == 2UL);
    QVERIFY(extr_bm.resource()->identifier() == "MemoryResourceNull");

    delete [] extr;
}

void dtkArrayTestCase::testView(void)
{
    dtk::Array<QString> a = {QLatin1String("foo"), QLatin1String("bar"), QLatin1String("toto"), QLatin1String("papa"), QLatin1String("mama"), QLatin1String("nini"), QLatin1String("dada"), QLatin1String("titi")};

    dtk::Array<QString>::ConstView a_cview = a.constView(0, 6, 2);
    QCOMPARE(a_cview.stride(), 2UL);
    QCOMPARE(a_cview.size(), 4UL);
    for (std::size_t i = 0; i < a_cview.size(); ++i) {
        QCOMPARE(a_cview[i], a[i * a_cview.stride()]);
    }

    dtk::Array<QString>::View a_view = a.view(0, 7, 3);
    QCOMPARE(a_view.stride(), 3UL);
    QCOMPARE(a_view.size(), 3UL);
    for (std::size_t i = 0; i < a_view.size(); ++i) {
        QCOMPARE(a_view[i], a[i * a_view.stride()]);
    }

    a_view = a.view(2, 7, 2);
    QCOMPARE(a_view.stride(), 2UL);
    QCOMPARE(a_view.size(), 3UL);

    for (std::size_t i = 0; i < a_view.size(); ++i) {
        QCOMPARE(a_view[i], a[2 + i * a_view.stride()]);
    }

    for (std::size_t i = 0; i < a_view.size(); ++i) {
        a_view[i] += i;
        QCOMPARE(a_view[i], a[2 + i * a_view.stride()]);
    }
}

void dtkArrayTestCase::testCompare(void)
{
    dtk::Array<double> array, array2, array3;

    QVERIFY(array == array2);

    array.append(1.0);
    array3.append(1.0);
    dtk::Array<double> array4(array);

    QVERIFY(array == array);
    QVERIFY(array != array2);
    QVERIFY(array == array3);
    QVERIFY(array == array4);

    array2.append(2.0);
    QVERIFY(array != array2);

    array2.append(1.0);
    QVERIFY(array != array2);

    for (std::size_t index = 0; index < 100; ++index) {
        array.append(index);
    }
    array2 = array;
    QVERIFY(array2 == array);
    QVERIFY(!(array2 != array));
}

void dtkArrayTestCase::testSearch(void)
{
    dtk::Array<double> array;
    for (std::size_t i = 0; i < 1000; ++i) {
        array.append(double(i));
    }

    QCOMPARE(array.indexOf(0.0), 0UL);
    QCOMPARE(array.indexOf(10.0), 10UL);
    QCOMPARE(array.indexOf(999.0), 999UL);
    QCOMPARE(array.indexOf(1000.0), -1UL);
    QCOMPARE(array.indexOf(10.0, 9), 10UL);
    QCOMPARE(array.indexOf(10.0, 10), 10UL);
    QCOMPARE(array.indexOf(10.0, 11), -1UL);
    QCOMPARE(array.indexOf(999.0, 0), 999UL);
    QCOMPARE(array.indexOf(998.0, 0), 998UL);

    QCOMPARE(array.lastIndexOf(0.0), 0UL);
    QCOMPARE(array.lastIndexOf(10.0), 10UL);
    QCOMPARE(array.lastIndexOf(999.0), 999UL);
    QCOMPARE(array.lastIndexOf(1000.0), -1UL);
    QCOMPARE(array.lastIndexOf(10.0, 9), -1UL);
    QCOMPARE(array.lastIndexOf(10.0, 10), 10UL);
    QCOMPARE(array.lastIndexOf(10.0, 11), 10UL);
    QCOMPARE(array.lastIndexOf(998.0, 1000-1), 998UL);

    QVERIFY(array.contains(0.0));
    QVERIFY(array.contains(10.0));
    QVERIFY(array.contains(999.0));
    QVERIFY(!array.contains(1000.0));
    QVERIFY(!array.contains(-1.0));

    array.append(500.0);
    QCOMPARE(array.count(0.0), 1UL);
    QCOMPARE(array.count(10.0), 1UL);
    QCOMPARE(array.count(500.0), 2UL);
    QCOMPARE(array.count(999.0), 1UL);
    QCOMPARE(array.count(1000.0), 0UL);

    QVERIFY(array.startsWith(0.0));
    QVERIFY(!array.startsWith(1.0));

    QVERIFY(array.endsWith(500.0));
    QVERIFY(!array.endsWith(1.0));

    QCOMPARE(array.first(), 0.0);
    QCOMPARE(array.last(), 500.0);
}

DTKCONTAINERSTEST_MAIN_NOGUI(dtkArrayTest, dtkArrayTestCase)

// ///////////////////////////////////////////////////////////////////
// Credits
// ///////////////////////////////////////////////////////////////////

/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt3D module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

//
// dtkArrayTest.cpp ends here
